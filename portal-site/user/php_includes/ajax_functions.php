<?php
require_once("connection.php");
@session_start();

function getOldPassword()
{
    $conn = connectDB();

	$user_id = $_SESSION["user_id"];
	$pass = md5($_POST["old_password"]);

	$query = "SELECT password FROM users WHERE id = :user_id";
	$stmt=$conn->prepare($query);

	$stmt->bindParam(":user_id",$user_id);

	$stmt->execute();

	if($result=$stmt->fetch(PDO::FETCH_OBJ)){
		if($pass == $result->password){
			echo "true";
			die();
		}else{
			echo "false";
			die();
		}
	}else{
		echo $user_id;
		die();
	}

}


if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'getOldPassword':
            getOldPassword();
            break;

        default:
            header("Location: ../index.php");
            break;
    }
}

?>