<?php
ini_set('display_errors', 1);
require_once("connection.php");
require_once("functions.php");
function generateUri($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function addUser()
{
    $conn = connectDB();

    $username = $_POST["username"];
    $password = md5($_POST["password"]);
    $type = $_POST["type"];
    $email = $_POST["email"];
    $phone_number = $_POST["phone_number"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];

    $query = "INSERT INTO users(username,password,type,email,phone_number,first_name,last_name) VALUES(:username,:password,:type,:email,:phone_number,:first_name,:last_name)";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":username", $username);
    $stmt->bindParam(":password", $password);
    $stmt->bindParam(":type", $type);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":phone_number", $phone_number);
    $stmt->bindParam(":first_name", $first_name);
    $stmt->bindParam(":last_name", $last_name);


    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function deleteUser()
{
    $conn = connectDB();

    $id = $_POST["id"];

    $query = "DELETE FROM users WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function updateUser()
{
    $conn = connectDB();

    $id = $_POST['id'];
    $type = $_POST["type"];
    $email = $_POST["email"];
    $phone_number = $_POST["phone_number"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];


    $query = "UPDATE users SET type = :type, email = :email, phone_number = :phone_number, first_name = :first_name, last_name = :last_name WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":id", $id);
    $stmt->bindParam(":type", $type);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":phone_number", $phone_number);
    $stmt->bindParam(":first_name", $first_name);
    $stmt->bindParam(":last_name", $last_name);

    $stmt->execute();

    $conn = null;
    header("Location: ../my_account.php?id=" . $id);
    die();
}

function addSite()
{
    $conn = connectDB();

    $user_id = $_POST["id"];
    $add_date = date("d-m-Y");
    $url = $_POST["url"];
    $name_en = $_POST["name_en"];
    $name_ro = "null";
    $country = $_POST["country"];
    $continent = $_POST["continent"];
    $slogan_en = $_POST["slogan_en"];
    $slogan_ro = "null";

    //update site where user_id = ... and id = ...
    $query1 = "INSERT INTO site(user_id,add_date,url,name_en,name_ro,country,continent,slogan_en,slogan_ro,";
    $query2 = " VALUES(:user_id,:add_date,:url,:name_en,:name_ro,:country,:continent,:slogan_en,:slogan_ro,";

    $CBRAND1 = intval($_POST["CBRAND1"]);
    $CBRAND2 = intval($_POST["CBRAND2"]);
    $CBRAND3 = intval($_POST["CBRAND3"]);
    $CBRAND4 = intval($_POST["CBRAND4"]);
    $CBRAND5 = intval($_POST["CBRAND5"]);
    $CBRAND6 = intval($_POST["CBRAND6"]);

    for ($i = 1; $i < 7; $i++) {
        $query1 .= "CBRAND" . $i . ",";
        $query2 .= ":CBRAND" . $i . ",";
    }

    $totalBrand = $CBRAND1 + $CBRAND2 + $CBRAND3 + $CBRAND4 + $CBRAND5 + $CBRAND6;
    // ===========================================================================

    $CMBRAND1 = intval($_POST["CMBRAND1"]);
    $CMBRAND2 = intval($_POST["CMBRAND2"]);
    $CMBRAND3 = intval($_POST["CMBRAND3"]);
    $CMBRAND4 = intval($_POST["CMBRAND4"]);
    $CMBRAND5 = intval($_POST["CMBRAND5"]);

    for ($i = 1; $i < 6; $i++) {
        $query1 .= "CMBRAND" . $i . ",";
        $query2 .= ":CMBRAND" . $i . ",";
    }

    $totalCMBrand = $CMBRAND1 + $CMBRAND2 + $CMBRAND3 + $CMBRAND4 + $CMBRAND5;
    // ===========================================================================

    $INFDESC1 = intval($_POST["INFDESC1"]);
    $INFDESC2 = intval($_POST["INFDESC2"]);
    $INFDESC3 = intval($_POST["INFDESC3"]);
    $INFDESC4 = intval($_POST["INFDESC4"]);
    $INFDESC5 = intval($_POST["INFDESC5"]);

    for ($i = 1; $i < 6; $i++) {
        $query1 .= "INFDESC" . $i . ",";
        $query2 .= ":INFDESC" . $i . ",";
    }


    $totalInfdesc = $INFDESC1 + $INFDESC2 + $INFDESC3 + $INFDESC4 + $INFDESC5;
    // ===========================================================================

    $INFASC1 = intval($_POST["INFASC1"]);
    $INFASC2 = intval($_POST["INFASC2"]);
    $INFASC3 = intval($_POST["INFASC3"]);

    $INFASC3_flag = 0;
    if ($INFASC3 > 0) {
        $INFASC3_flag = 1;
    }

    for ($i = 1; $i < 4; $i++) {
        $query1 .= "INFASC" . $i . ",";
        $query2 .= ":INFASC" . $i . ",";
    }

    $totalInfasc = $INFASC2 + $INFASC2 + $INFASC3_flag;
    // ===========================================================================

    $INFLAT1 = intval($_POST["INFLAT1"]);
    $INFLAT2 = intval($_POST["INFLAT2"]);

    for ($i = 1; $i < 3; $i++) {
        $query1 .= "INFLAT" . $i . ",";
        $query2 .= ":INFLAT" . $i . ",";
    }

    $totalInflat = $INFLAT1 + $INFLAT2;
    // ===========================================================================

    $INASINC1 = intval($_POST["INASINC1"]);
    $INASINC2 = intval($_POST["INASINC2"]);
    $INASINC3 = intval($_POST["INASINC3"]);
    $INASINC4 = intval($_POST["INASINC4"]);
    $INASINC5 = intval($_POST["INASINC5"]);
    $INASINC6 = intval($_POST["INASINC6"]);
    $INASINC7 = intval($_POST["INASINC7"]);

    $INASINC2_flag = 0;
    if ($INASINC2 > 0) {
        $INASINC2_flag = 1;
    }

    $INASINC3_flag = 0;
    if ($INASINC3 > 0) {
        $INASINC3_flag = 1;
    }

    for ($i = 1; $i < 8; $i++) {
        $query1 .= "INASINC" . $i . ",";
        $query2 .= ":INASINC" . $i . ",";
    }

    $totalInasinc = $INASINC1 + $INASINC2_flag + $INASINC3_flag + $INASINC4 + $INASINC5 + $INASINC6 + $INASINC7;
    // ===========================================================================

    $INSINC1 = intval($_POST["INSINC1"]);
    $INSINC2 = intval($_POST["INSINC2"]);
    $INSINC3 = intval($_POST["INSINC3"]);

    $INSINC2_flag = 0;
    if ($INSINC2 > 0) {
        $INSINC2_flag = 1;
    }

    for ($i = 1; $i < 4; $i++) {
        $query1 .= "INSINC" . $i . ",";
        $query2 .= ":INSINC" . $i . ",";
    }

    $totalInsinc = $INSINC1 + $INSINC2_flag + $INSINC3;
    // ===========================================================================

    $PREZ1 = intval($_POST["PREZ1"]);
    $PREZ2 = intval($_POST["PREZ2"]);

    for ($i = 1; $i < 3; $i++) {
        $query1 .= "PREZ" . $i . ",";
        $query2 .= ":PREZ" . $i . ",";
    }

    $PREZ2_flag = 0;
    if ($PREZ2 > 0) {
        $PREZ2_flag = 1;
    }

    $totalPrez = $PREZ1 + $PREZ2_flag;

    // ===========================================================================

    $MULTIM1 = intval($_POST["MULTIM1"]);
    $MULTIM2 = intval($_POST["MULTIM2"]);
    $MULTIM3 = intval($_POST["MULTIM3"]);
    $MULTIM4 = intval($_POST["MULTIM4"]);

    for ($i = 1; $i < 5; $i++) {
        $query1 .= "MULTIM" . $i . ",";
        $query2 .= ":MULTIM" . $i . ",";
    }

    $totalMultim = $MULTIM1 + $MULTIM2 + $MULTIM3 + $MULTIM4;
    // ===========================================================================

    $ACCES1 = intval($_POST["ACCES1"]);
    $ACCES2 = intval($_POST["ACCES2"]);
    $ACCES3 = intval($_POST["ACCES3"]);
    $ACCES4 = intval($_POST["ACCES4"]);
    $ACCES5 = intval($_POST["ACCES5"]);

    for ($i = 1; $i < 6; $i++) {
        $query1 .= "ACCES" . $i . ",";
        $query2 .= ":ACCES" . $i . ",";
    }
    $ACCES3_flag = 0;
    if ($ACCES3 > 0) {
        $ACCES3_flag = 1;
    }
    $totalAcces = $ACCES1 + $ACCES2 + $ACCES3_flag + $ACCES4;
    // ===========================================================================

    $NAVIG1 = intval($_POST["NAVIG1"]);
    $NAVIG2 = intval($_POST["NAVIG2"]);
    $NAVIG3 = intval($_POST["NAVIG3"]);
    $NAVIG4 = intval($_POST["NAVIG4"]);

    for ($i = 1; $i < 5; $i++) {
        $query1 .= "NAVIG" . $i . ",";
        $query2 .= ":NAVIG" . $i . ",";
    }

    $totalNavig = $NAVIG1 + $NAVIG2 + $NAVIG3 + $NAVIG4;
    // ===========================================================================

    $ACTUALIZ = intval($_POST["ACTUALIZ"]);
    $query1 .= "ACTUALIZ,";
    $query2 .= ":ACTUALIZ,";

    $VIZIBIL = intval($_POST["VIZIBIL"]);
    $query1 .= "VIZIBIL,";
    $query2 .= ":VIZIBIL,";

    //Brand design total sum
    $brand_design = $totalBrand;
    $query1 .= "brand_design,";
    $query2 .= ":brand_design,";

    //Brand behaviour total sum
    $brand_behaviour = $totalCMBrand;
    $query1 .= "brand_behaviour,";
    $query2 .= ":brand_behaviour,";

    //Brand information and communication
    $brand_communication = $totalInfdesc + $totalInfasc + $totalInflat + $totalInasinc + $totalInsinc;
    $query1 .= "brand_communication,";
    $query2 .= ":brand_communication,";

    //Site Delviery
    $site_delivery = $totalPrez + $totalMultim + $totalAcces + $totalNavig;
    $query1 .= "site_delivery,";
    $query2 .= ":site_delivery,";

    $total_score = $brand_design + $brand_behaviour + $brand_communication + $site_delivery;
    $query1 .= "total_score)";
    $query2 .= ":total_score)";

    $query = $query1 . $query2;

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":user_id", $user_id);
    $stmt->bindParam(":add_date", $add_date);
    $stmt->bindParam(":url", $url);
    $stmt->bindParam(":name_en", $name_en);
    $stmt->bindParam(":name_ro", $name_ro);
    $stmt->bindParam(":country", $country);
    $stmt->bindParam(":continent", $continent);
    $stmt->bindParam(":slogan_en", $slogan_en);
    $stmt->bindParam(":slogan_ro", $slogan_ro);

    //THE REST OF THE PARAMS
    $stmt->bindParam(":CBRAND1", $CBRAND1);
    $stmt->bindParam(":CBRAND2", $CBRAND2);
    $stmt->bindParam(":CBRAND3", $CBRAND3);
    $stmt->bindParam(":CBRAND4", $CBRAND4);
    $stmt->bindParam(":CBRAND5", $CBRAND5);
    $stmt->bindParam(":CBRAND6", $CBRAND6);

    $stmt->bindParam(":CMBRAND1", $CMBRAND1);
    $stmt->bindParam(":CMBRAND2", $CMBRAND2);
    $stmt->bindParam(":CMBRAND3", $CMBRAND3);
    $stmt->bindParam(":CMBRAND4", $CMBRAND4);
    $stmt->bindParam(":CMBRAND5", $CMBRAND5);

    $stmt->bindParam(":INFDESC1", $INFDESC1);
    $stmt->bindParam(":INFDESC2", $INFDESC2);
    $stmt->bindParam(":INFDESC3", $INFDESC3);
    $stmt->bindParam(":INFDESC4", $INFDESC4);
    $stmt->bindParam(":INFDESC5", $INFDESC5);

    $stmt->bindParam(":INFASC1", $INFASC1);
    $stmt->bindParam(":INFASC2", $INFASC2);
    $stmt->bindParam(":INFASC3", $INFASC3);

    $stmt->bindParam(":INFLAT1", $INFLAT1);
    $stmt->bindParam(":INFLAT2", $INFLAT2);

    $stmt->bindParam(":INASINC1", $INASINC1);
    $stmt->bindParam(":INASINC2", $INASINC2);
    $stmt->bindParam(":INASINC3", $INASINC3);
    $stmt->bindParam(":INASINC4", $INASINC4);
    $stmt->bindParam(":INASINC5", $INASINC5);
    $stmt->bindParam(":INASINC6", $INASINC6);
    $stmt->bindParam(":INASINC7", $INASINC7);

    $stmt->bindParam(":INSINC1", $INSINC1);
    $stmt->bindParam(":INSINC2", $INSINC2);
    $stmt->bindParam(":INSINC3", $INSINC3);

    $stmt->bindParam(":PREZ1", $PREZ1);
    $stmt->bindParam(":PREZ2", $PREZ2);

    $stmt->bindParam(":MULTIM1", $MULTIM1);
    $stmt->bindParam(":MULTIM2", $MULTIM2);
    $stmt->bindParam(":MULTIM3", $MULTIM3);
    $stmt->bindParam(":MULTIM4", $MULTIM4);

    $stmt->bindParam(":ACCES1", $ACCES1);
    $stmt->bindParam(":ACCES2", $ACCES2);
    $stmt->bindParam(":ACCES3", $ACCES3);
    $stmt->bindParam(":ACCES4", $ACCES4);
    $stmt->bindParam(":ACCES5", $ACCES5);

    $stmt->bindParam(":NAVIG1", $NAVIG1);
    $stmt->bindParam(":NAVIG2", $NAVIG2);
    $stmt->bindParam(":NAVIG3", $NAVIG3);
    $stmt->bindParam(":NAVIG4", $NAVIG4);

    $stmt->bindParam(":ACTUALIZ", $ACTUALIZ);

    $stmt->bindParam(":VIZIBIL", $VIZIBIL);

    $stmt->bindParam(":brand_design", $brand_design);

    $stmt->bindParam(":brand_behaviour", $brand_behaviour);

    $stmt->bindParam(":brand_communication", $brand_communication);

    $stmt->bindParam(":site_delivery", $site_delivery);

    $stmt->bindParam(":total_score", $total_score);


    $stmt->execute();

    $conn = null;
    header("Location: ../sites.php");
    die();
}

function deleteSite()
{
    $conn = connectDB();

    $id = $_POST["id"];

    $query = "DELETE FROM site WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../sites.php");
    die();
}

function updateSite()
{
    $conn = connectDB();

    $i = 0;

    $id = $_POST["id"];
    $user_id = $_SESSION['user_id'];

    $query = "UPDATE site SET ";

    //update site where user_id = ... and id = ...

    $CBRAND1 = intval($_POST["CBRAND1"]);
    $CBRAND2 = intval($_POST["CBRAND2"]);
    $CBRAND3 = intval($_POST["CBRAND3"]);
    $CBRAND4 = intval($_POST["CBRAND4"]);
    $CBRAND5 = intval($_POST["CBRAND5"]);
    $CBRAND6 = intval($_POST["CBRAND6"]);

    for ($i = 1; $i < 7; $i++) {
        $query .= "CBRAND" . $i . " = :CBRAND" . $i . ", ";
    }

    $totalBrand = $CBRAND1 + $CBRAND2 + $CBRAND3 + $CBRAND4 + $CBRAND5 + $CBRAND6;
    // ===========================================================================

    $CMBRAND1 = intval($_POST["CMBRAND1"]);
    $CMBRAND2 = intval($_POST["CMBRAND2"]);
    $CMBRAND3 = intval($_POST["CMBRAND3"]);
    $CMBRAND4 = intval($_POST["CMBRAND4"]);
    $CMBRAND5 = intval($_POST["CMBRAND5"]);

    for ($i = 1; $i < 6; $i++) {
        $query .= "CMBRAND" . $i . " = :CMBRAND" . $i . ", ";
    }

    $totalCMBrand = $CMBRAND1 + $CMBRAND2 + $CMBRAND3 + $CMBRAND4 + $CMBRAND5;
    // ===========================================================================

    $INFDESC1 = intval($_POST["INFDESC1"]);
    $INFDESC2 = intval($_POST["INFDESC2"]);
    $INFDESC3 = intval($_POST["INFDESC3"]);
    $INFDESC4 = intval($_POST["INFDESC4"]);
    $INFDESC5 = intval($_POST["INFDESC5"]);

    for ($i = 1; $i < 6; $i++) {
        $query .= "INFDESC" . $i . " = :INFDESC" . $i . ", ";
    }

    $totalInfdesc = $INFDESC1 + $INFDESC2 + $INFDESC3 + $INFDESC4 + $INFDESC5;
    // ===========================================================================

    $INFASC1 = intval($_POST["INFASC1"]);
    $INFASC2 = intval($_POST["INFASC2"]);
    $INFASC3 = intval($_POST["INFASC3"]);

    for ($i = 1; $i < 4; $i++) {
        $query .= "INFASC" . $i . " = :INFASC" . $i . ", ";
    }

    $INFASC3_flag = 0;
    if ($INFASC3 > 0) {
        $INFASC3_flag = 1;
    }

    $totalInfasc = $INFASC2 + $INFASC2 + $INFASC3_flag;
    // ===========================================================================

    $INFLAT1 = intval($_POST["INFLAT1"]);
    $INFLAT2 = intval($_POST["INFLAT2"]);

    for ($i = 1; $i < 3; $i++) {
        $query .= "INFLAT" . $i . " = :INFLAT" . $i . ", ";
    }

    $totalInflat = $INFLAT1 + $INFLAT2;
    // ===========================================================================

    $INASINC1 = intval($_POST["INASINC1"]);
    $INASINC2 = intval($_POST["INASINC2"]);
    $INASINC3 = intval($_POST["INASINC3"]);
    $INASINC4 = intval($_POST["INASINC4"]);
    $INASINC5 = intval($_POST["INASINC5"]);
    $INASINC6 = intval($_POST["INASINC6"]);
    $INASINC7 = intval($_POST["INASINC7"]);

    for ($i = 1; $i < 8; $i++) {
        $query .= "INASINC" . $i . " = :INASINC" . $i . ", ";
    }

    $INASINC2_flag = 0;
    $INASINC3_flag = 0;

    if ($INASINC2 > 0) {
        $INASINC2_flag = 1;
    }

    if ($INASINC3 > 0) {
        $INASINC3_flag = 1;
    }

    $totalInasinc = $INASINC1 + $INASINC2_flag + $INASINC3_flag + $INASINC4 + $INASINC5 + $INASINC6 + $INASINC7;
    // ===========================================================================

    $INSINC1 = intval($_POST["INSINC1"]);
    $INSINC2 = intval($_POST["INSINC2"]);
    $INSINC3 = intval($_POST["INSINC3"]);

    for ($i = 1; $i < 4; $i++) {
        $query .= "INSINC" . $i . " = :INSINC" . $i . ", ";
    }

    $INSINC2_flag = 0;
    if ($INSINC2 > 0) {
        $INSINC2_flag = 1;
    }

    $totalInsinc = $INSINC1 + $INSINC2_flag + $INSINC3;
    // ===========================================================================

    $PREZ1 = intval($_POST["PREZ1"]);
    $PREZ2 = intval($_POST["PREZ2"]);

    for ($i = 1; $i < 3; $i++) {
        $query .= "PREZ" . $i . " = :PREZ" . $i . ", ";
    }

    $PREZ2_flag = 0;
    if ($PREZ2 > 0) {
        $PREZ2_flag = 1;
    }

    $totalPrez = $PREZ1 + $PREZ2_flag;

    // ===========================================================================

    $MULTIM1 = intval($_POST["MULTIM1"]);
    $MULTIM2 = intval($_POST["MULTIM2"]);
    $MULTIM3 = intval($_POST["MULTIM3"]);
    $MULTIM4 = intval($_POST["MULTIM4"]);

    for ($i = 1; $i < 5; $i++) {
        $query .= "MULTIM" . $i . " = :MULTIM" . $i . ", ";
    }

    $totalMultim = $MULTIM1 + $MULTIM2 + $MULTIM3 + $MULTIM4;
    // ===========================================================================

    $ACCES1 = intval($_POST["ACCES1"]);
    $ACCES2 = intval($_POST["ACCES2"]);
    $ACCES3 = intval($_POST["ACCES3"]);
    $ACCES4 = intval($_POST["ACCES4"]);
    $ACCES5 = intval($_POST["ACCES5"]);

    for ($i = 1; $i < 6; $i++) {
        $query .= "ACCES" . $i . " = :ACCES" . $i . ", ";
    }

    $ACCES3_flag = 0;
    if ($ACCES3 > 0) {
        $ACCES3_flag = 1;
    }

    $totalAcces = $ACCES1 + $ACCES2 + $ACCES3_flag + $ACCES4;
    // ===========================================================================

    $NAVIG1 = intval($_POST["NAVIG1"]);
    $NAVIG2 = intval($_POST["NAVIG2"]);
    $NAVIG3 = intval($_POST["NAVIG3"]);
    $NAVIG4 = intval($_POST["NAVIG4"]);

    for ($i = 1; $i < 5; $i++) {
        $query .= "NAVIG" . $i . " = :NAVIG" . $i . ", ";
    }

    $totalNavig = $NAVIG1 + $NAVIG2 + $NAVIG3 + $NAVIG4;
    // ===========================================================================

    $ACTUALIZ = intval($_POST["ACTUALIZ"]);
    $query .= "ACTUALIZ= :ACTUALIZ, ";

    $VIZIBIL = intval($_POST["VIZIBIL"]);
    $query .= "VIZIBIL= :VIZIBIL, ";

    //Brand design total sum
    $brand_design = $totalBrand;
    $query .= "brand_design= :brand_design, ";

    //Brand behaviour total sum
    $brand_behaviour = $totalCMBrand;
    $query .= "brand_behaviour= :brand_behaviour, ";

    //Brand information and communication
    $brand_communication = $totalInfdesc + $totalInfasc + $totalInflat + $totalInasinc + $totalInsinc;
    $query .= "brand_communication= :brand_communication, ";

    //Site Delviery
    $site_delivery = $totalPrez + $totalMultim + $totalAcces + $totalNavig;

    $query .= "site_delivery= :site_delivery,";

    $total_score = $brand_design + $brand_behaviour + $brand_communication + $site_delivery;
    $query .= "total_score= :total_score";

    $query .= " WHERE id= :id AND user_id = :user_id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":CBRAND1", $CBRAND1);
    $stmt->bindParam(":CBRAND2", $CBRAND2);
    $stmt->bindParam(":CBRAND3", $CBRAND3);
    $stmt->bindParam(":CBRAND4", $CBRAND4);
    $stmt->bindParam(":CBRAND5", $CBRAND5);
    $stmt->bindParam(":CBRAND6", $CBRAND6);

    $stmt->bindParam(":CMBRAND1", $CMBRAND1);
    $stmt->bindParam(":CMBRAND2", $CMBRAND2);
    $stmt->bindParam(":CMBRAND3", $CMBRAND3);
    $stmt->bindParam(":CMBRAND4", $CMBRAND4);
    $stmt->bindParam(":CMBRAND5", $CMBRAND5);

    $stmt->bindParam(":INFDESC1", $INFDESC1);
    $stmt->bindParam(":INFDESC2", $INFDESC2);
    $stmt->bindParam(":INFDESC3", $INFDESC3);
    $stmt->bindParam(":INFDESC4", $INFDESC4);
    $stmt->bindParam(":INFDESC5", $INFDESC5);

    $stmt->bindParam(":INFASC1", $INFASC1);
    $stmt->bindParam(":INFASC2", $INFASC2);
    $stmt->bindParam(":INFASC3", $INFASC3);

    $stmt->bindParam(":INFLAT1", $INFLAT1);
    $stmt->bindParam(":INFLAT2", $INFLAT2);

    $stmt->bindParam(":INASINC1", $INASINC1);
    $stmt->bindParam(":INASINC2", $INASINC2);
    $stmt->bindParam(":INASINC3", $INASINC3);
    $stmt->bindParam(":INASINC4", $INASINC4);
    $stmt->bindParam(":INASINC5", $INASINC5);
    $stmt->bindParam(":INASINC6", $INASINC6);
    $stmt->bindParam(":INASINC7", $INASINC7);

    $stmt->bindParam(":INSINC1", $INSINC1);
    $stmt->bindParam(":INSINC2", $INSINC2);
    $stmt->bindParam(":INSINC3", $INSINC3);

    $stmt->bindParam(":PREZ1", $PREZ1);
    $stmt->bindParam(":PREZ2", $PREZ2);

    $stmt->bindParam(":MULTIM1", $MULTIM1);
    $stmt->bindParam(":MULTIM2", $MULTIM2);
    $stmt->bindParam(":MULTIM3", $MULTIM3);
    $stmt->bindParam(":MULTIM4", $MULTIM4);

    $stmt->bindParam(":ACCES1", $ACCES1);
    $stmt->bindParam(":ACCES2", $ACCES2);
    $stmt->bindParam(":ACCES3", $ACCES3);
    $stmt->bindParam(":ACCES4", $ACCES4);
    $stmt->bindParam(":ACCES5", $ACCES5);

    $stmt->bindParam(":NAVIG1", $NAVIG1);
    $stmt->bindParam(":NAVIG2", $NAVIG2);
    $stmt->bindParam(":NAVIG3", $NAVIG3);
    $stmt->bindParam(":NAVIG4", $NAVIG4);

    $stmt->bindParam(":ACTUALIZ", $ACTUALIZ);

    $stmt->bindParam(":VIZIBIL", $VIZIBIL);

    $stmt->bindParam(":brand_design", $brand_design);

    $stmt->bindParam(":brand_behaviour", $brand_behaviour);

    $stmt->bindParam(":brand_communication", $brand_communication);

    $stmt->bindParam(":site_delivery", $site_delivery);

    $stmt->bindParam(":total_score", $total_score);

    $stmt->bindParam(":id", $id);

    $stmt->bindParam(":user_id", $user_id);

    echo $query;

    $stmt->execute();

    $conn = null;
    header("Location: ../view_site.php?id=" . $id);
    die();

}

function updatePassword()
{
    $conn = connectDB();

    $id = $_SESSION["user_id"];

    $password = md5($_POST["repeat_new_pass"]);

    $query = "UPDATE users SET password=:password WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":password", $password);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../logout.php");
    die();
}

?>