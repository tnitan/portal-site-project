<?php
require_once("post_functions.php");

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'addUser':
            addUser();
            break;

        case 'deleteUser':
            deleteUser();
            break;

        case 'updateUser':
            updateUser();
            break;

        case 'addSite':
            addSite();
            break;

        case 'deleteSite':
            deleteSite();
            break;

        case 'updateSite':
            updateSite();
            break;

        case 'updatePassword':
            updatePassword();
            break;

        // default:
        // 	header("Location: ../index.php");
        // 	break;
    }
}
?>