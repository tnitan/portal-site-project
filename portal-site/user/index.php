<?php
require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$site_delivery = getSitesUser($_SESSION["user_id"], "site_delivery");
$topSiteDelivery = getTopDelivery($_SESSION["user_id"], "site_delivery");

$siteDelivery = "";
for ($i = 0; $i < count($site_delivery); $i++) {
    if ($site_delivery[$i]->id != $topSiteDelivery->id) {
        if ($i == 0) {
            $siteDelivery .= "" . $site_delivery[$i]->name_en . "':" . $site_delivery[$i]->site_delivery . "";
        } else {
            $siteDelivery .= ",'" . $site_delivery[$i]->name_en . "':" . $site_delivery[$i]->site_delivery . "";
        }
    }
}

$brand_communication = getSitesUser($_SESSION["user_id"], "brand_communication");
$topBrandCommunication = getTopDelivery($_SESSION["user_id"], "brand_communication");

$brandCommunication = "";
for ($i = 0; $i < count($brand_communication); $i++) {
    if ($brand_communication[$i]->id != $topBrandCommunication->id) {
        if ($i == 0) {
            $brandCommunication .= "" . $brand_communication[$i]->name_en . "':" . $brand_communication[$i]->brand_communication . "";
        } else {
            $brandCommunication .= ",'" . $brand_communication[$i]->name_en . "':" . $brand_communication[$i]->brand_communication . "";
        }
    }
}

$brand_behaviour = getSitesUser($_SESSION["user_id"], "brand_behaviour");
$topBrandBehaviour = getTopDelivery($_SESSION["user_id"], "brand_behaviour");

$brandBehaviour = "";
for ($i = 0; $i < count($brand_behaviour); $i++) {
    if ($brand_behaviour[$i]->id != $topBrandBehaviour->id) {
        if ($i == 0) {
            $brandBehaviour .= "" . $brand_behaviour[$i]->name_en . "':" . $brand_behaviour[$i]->brand_behaviour . "";
        } else {
            $brandBehaviour .= ",'" . $brand_behaviour[$i]->name_en . "':" . $brand_behaviour[$i]->brand_behaviour . "";
        }
    }
}

$brand_design = getSitesUser($_SESSION["user_id"], "brand_design");
$topBrandDesign = getTopDelivery($_SESSION["user_id"], "brand_design");

$brandDesign = "";
for ($i = 0; $i < count($brand_design); $i++) {
    if ($brand_design[$i]->id != $topBrandDesign->id) {
        if ($i == 0) {
            $brandDesign .= "" . $brand_design[$i]->name_en . "':" . $brand_design[$i]->brand_design . "";
        } else {
            $brandDesign .= ",'" . $brand_design[$i]->name_en . "':" . $brand_design[$i]->brand_design . "";
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Welcome - Dashboard </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>
    <style type="text/css">
        .boomChart:hover {
            cursor: pointer;
            color: green;
            text-decoration: underline;
        }
    </style>
    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="active">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="my_account.php">
                    <i class="menu-icon fa 	fa-lemon-o  red"></i>
                    <span class="menu-text"> My Account </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="sites.php">
                    <i class="menu-icon fa 	fa-external-link  green"></i>
                    <span class="menu-text"> My Sites </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="active">Dashboard</li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 3) {
                            echo "user";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    Dashboard
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Bine ai venit
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="col-xs-12 col-sm-3">

                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                <i class="ace-icon fa fa-star orange"></i>
                                Top 10 By <b><u>Site Delivery</u></b>
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-bordered table-striped">
                                    <thead class="thin-border-bottom">
                                    <tr>
                                        <th>
                                            #rank
                                        </th>
                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>name
                                        </th>

                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>points
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php foreach ($site_delivery as $key => $value): ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>

                                            <td><?php echo strip_tags($value->name_en); ?></td>

                                            <td>
                                                <b class="green"><?php echo strip_tags($value->site_delivery); ?> pts.</b>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>

                                    </tbody>
                                </table>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>

                    <div class="col-xs-12 col-sm-3">

                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                <i class="ace-icon fa fa-star orange"></i>
                                Top 10 By <b><u>Brand Communication</u></b>
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-bordered table-striped">
                                    <thead class="thin-border-bottom">
                                    <tr>
                                        <th>
                                            #rank
                                        </th>
                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>name
                                        </th>

                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>points
                                        </th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php foreach ($brand_communication as $key => $value): ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>

                                            <td><?php echo strip_tags($value->name_en); ?></td>

                                            <td>
                                                <b class="green"><?php echo strip_tags($value->brand_communication); ?> pts.</b>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>

                                    </tbody>
                                </table>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>

                    <div class="col-xs-12 col-sm-3">

                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                <i class="ace-icon fa fa-star orange"></i>
                                Top 10 By <b><u>Brand Behaviour</u></b>
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-bordered table-striped">
                                    <thead class="thin-border-bottom">
                                    <tr>
                                        <th>
                                            #rank
                                        </th>
                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>name
                                        </th>

                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>points
                                        </th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php foreach ($brand_behaviour as $key => $value): ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>

                                            <td><?php echo strip_tags($value->name_en); ?></td>

                                            <td>
                                                <b class="green"><?php echo strip_tags($value->brand_behaviour); ?> pts.</b>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>

                                    </tbody>
                                </table>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>

                    <div class="col-xs-12 col-sm-3">

                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                <i class="ace-icon fa fa-star orange"></i>
                                Top 10 By <b><u>Brand Design</u></b>
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-bordered table-striped">
                                    <thead class="thin-border-bottom">
                                    <tr>
                                        <th>
                                            #rank
                                        </th>
                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>name
                                        </th>

                                        <th>
                                            <i class="ace-icon fa fa-caret-right blue"></i>points
                                        </th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php foreach ($brand_design as $key => $value): ?>
                                        <tr>
                                            <td><?php echo $key + 1; ?></td>

                                            <td><?php echo strip_tags($value->name_en); ?></td>

                                            <td>
                                                <b class="green"><?php echo strip_tags($value->brand_design); ?> pts.</b>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>

                                    </tbody>
                                </table>
                            </div><!-- /.widget-main -->
                        </div><!-- /.widget-body -->

                    </div>
                    <div class="col-xs-12">
                        <div class="graph" id="chart1"></div>
                    </div>
                    <br/>

                    <div class="col-xs-12" style="margin-top: 40px;">
                        <div class="graph" id="chart2"></div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 40px;">
                        <div class="graph" id="chart3"></div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 40px;">
                        <div class="graph" id="chart4"></div>
                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->

    <?php include("./html_includes/footer.html"); ?>
    <div class="dialog-message" style="min-width:300px;min-height: 300px;">
    </div><!-- #dialog-message -->
    <a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/jquery.easypiechart.min.js"></script>
<script src="dist/js/jquery.sparkline.min.js"></script>
<script src="dist/js/flot/jquery.flot.min.js"></script>
<script src="dist/js/flot/jquery.flot.pie.min.js"></script>
<script src="dist/js/flot/jquery.flot.resize.min.js"></script>
<script src="dist/js/jquery-ui.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="./dist/js/graphite.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">


    $(document).ready(function () {
        var myData1 = {<?="'" . $topSiteDelivery->name_en . "':" . $topSiteDelivery->site_delivery . $siteDelivery ?>};

        var myOptions1 = {
            'height': 400,
            'title': 'Site Delivery',
            'width': 900,
            barColor: 'linear-gradient(rgb(144, 164, 237), rgb(122, 139, 204))',
            'fixPadding': 18
        };

        var myData2 = {<?="'" . $topBrandCommunication->name_en . "':" . $topBrandCommunication->brand_communication . $brandCommunication ?>};

        var myOptions2 = {
            'height': 400,
            'title': 'Brand Communication',
            'width': 900,
            barColor: 'linear-gradient(rgb(144, 164, 237), rgb(122, 139, 204))',
            'fixPadding': 18,
            'barFont': [0, 12, "bold"],
            'labelFont': [0, 13, 0]
        };

        var myData3 = {
            '<?php echo strip_tags($topBrandBehaviour->name_en) ?>': <?php echo strip_tags($topBrandBehaviour->brand_behaviour) ?>
            <?php echo strip_tags($brandBehaviour) ?>
        };

        var myOptions3 = {
            'height': 400,
            'title': 'Brand Behaviour',
            'width': 900,
            barColor: 'linear-gradient(rgb(144, 164, 237), rgb(122, 139, 204))',
            'fixPadding': 18,
            'barFont': [0, 12, "bold"],
            'labelFont': [0, 13, 0]
        };

        var myData4 = {
            '<?php echo strip_tags($topBrandDesign->name_en) ?>': <?php echo strip_tags($topBrandDesign->brand_design) ?>
            <?php echo strip_tags($brandDesign) ?>
        };

        var myOptions4 = {
            'height': 400,
            'title': 'Brand Design',
            'width': 900,
            barColor: 'linear-gradient(rgb(144, 164, 237), rgb(122, 139, 204))',
            'fixPadding': 18,
            'barFont': [0, 12, "bold"],
            'labelFont': [0, 13, 0]
        };

        graphite(myData1, myOptions1, chart1);
        graphite(myData2, myOptions2, chart2);
        graphite(myData3, myOptions3, chart3);
        graphite(myData4, myOptions4, chart4);

        $(document).on("click", ".boomChart", function () {
            var total_score, brand_design, brand_behaviour, brand_communication, site_delivery;

            total_score = parseInt($(this).siblings(".total_score").val());
            brand_design = parseInt($(this).siblings(".brand_design").val());
            brand_behaviour = parseInt($(this).siblings(".brand_behaviour").val());
            brand_communication = parseInt($(this).siblings(".brand_communication").val());
            site_delivery = parseInt($(this).siblings(".site_delivery").val());
            var nameSite = $(this).siblings(".siteName").val();
            $("#nameSite").html(nameSite);

            constructChart(total_score, brand_design, brand_behaviour, brand_communication, site_delivery);

        });

        function constructChart(total_score, brand_design, brand_behaviour, brand_communication, site_delivery) {
            $("#piechart-placeholder").html("");

            $.resize.throttleWindow = false;

            var procentBrandDesign, procentBrandBehaviour, procentBrandCommunication, procentSiteDelivery;

            procentBrandDesign = 100 * brand_design / total_score;
            if (procentBrandDesign < 1 && procentBrandDesign > 0) {
                procentBrandDesign = 1;
            }

            procentBrandBehaviour = 100 * brand_behaviour / total_score;
            if (procentBrandBehaviour < 1 && procentBrandBehaviour > 0) {
                procentBrandBehaviour = 1;
            }

            procentBrandCommunication = 100 * brand_communication / total_score;
            if (procentBrandCommunication < 1 && procentBrandCommunication > 0) {
                procentBrandCommunication = 1;
            }

            procentSiteDelivery = 100 * site_delivery / total_score;
            if (procentSiteDelivery < 1 && procentSiteDelivery > 0) {
                procentSiteDelivery = 1;
            }

            console.log();

            var placeholder = $('#piechart-placeholder').css({'width': '100%', 'min-height': '300px'});
            var data = [
                {label: "Brand Design", data: procentBrandDesign, color: "#68BC31"},
                {label: "Brand Behaviour", data: procentBrandBehaviour, color: "#2091CF"},
                {label: "Brand Communication", data: procentBrandCommunication, color: "#AF4E96"},
                {label: "Site Delivery", data: procentSiteDelivery, color: "#DA5430"}
            ];

            drawPieChart(placeholder, data);
            placeholder.data('chart', data);
            placeholder.data('draw', drawPieChart);

            //pie chart tooltip example
            var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
            var previousPoint = null;

            placeholder.on('plothover', function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.seriesIndex) {
                        previousPoint = item.seriesIndex;
                        var tip = item.series['label'] + " : " + item.series['percent'] + '%';
                        $tooltip.show().children(0).text(tip);
                    }
                    $tooltip.css({top: pos.pageY + 10, left: pos.pageX + 10});
                } else {
                    $tooltip.hide();
                    previousPoint = null;
                }

            });
        }

        function drawPieChart(placeholder, data, position) {
            $.plot(placeholder, data, {
                series: {
                    pie: {
                        show: true,
                        tilt: 0.8,
                        highlight: {
                            opacity: 0.25
                        },
                        stroke: {
                            color: '#fff',
                            width: 2
                        },
                        startAngle: 2
                    }
                },
                legend: {
                    show: true,
                    position: position || "ne",
                    labelBoxBorderColor: null,
                    margin: [-30, 15]
                }
                ,
                grid: {
                    hoverable: true,
                    clickable: true
                }
            })
        }

    });
</script>

</body>
</html>