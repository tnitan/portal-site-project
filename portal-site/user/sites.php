<?php

require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$user = getUser($_SESSION["user_id"]);
$sites = getUserSites($_SESSION["user_id"]);
// var_dump($sites);
// die();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Add Sites - <?php echo $user->username ?> </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="my_account.php">
                    <i class="menu-icon fa 	fa-lemon-o  red"></i>
                    <span class="menu-text"> My Account </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="active">
                <a href="sites.php">
                    <i class="menu-icon fa 	fa-external-link  green"></i>
                    <span class="menu-text"> My Sites </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="active"><?php echo strip_tags($user->first_name) ?><?php echo strip_tags($user->last_name) ?></li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 3) {
                            echo "user";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    <?php echo $user->username ?>
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        My sites
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="col-xs-12">
                        <div class="col-sm-12 col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">Add new site </h4>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main" style="overflow: hidden">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="./php_includes/post_switch.php">
                                            <input type="hidden" value="addSite" name="action"/>
                                            <input type="hidden" value="<?php echo strip_tags($user->id) ?>" name="id"/>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="name_en" class="col-sm-3 control-label no-padding-right">
                                                    Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="name_en" value="" required
                                                           class="col-xs-12" placeholder="Name" id="name_en">
                                                </div>
                                            </div>

                                            <!-- <div class="form-group col-xs-12 col-sm-12">
                                                <label for="name_ro" class="col-sm-3 control-label no-padding-right"> Name Ro: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="name_ro" value="" required class="col-xs-12" placeholder="Name Ro" id="name_ro">
                                                </div>
                                            </div> -->

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="url" class="col-sm-3 control-label no-padding-right">
                                                    URL: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="url" value="" required class="col-xs-12"
                                                           placeholder="URL" id="url">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="country" class="col-sm-3 control-label no-padding-right">
                                                    Country: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="country" value="" required
                                                           class="col-xs-12" placeholder="Country" id="country">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="continent" class="col-sm-3 control-label no-padding-right">
                                                    Continent: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="continent" value="" required
                                                           class="col-xs-12" placeholder="Continent" id="continent">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="slogan_en" class="col-sm-3 control-label no-padding-right">
                                                    Slogan: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="slogan_en" value="" required
                                                           class="col-xs-12" placeholder="Slogan" id="slogan_en">
                                                </div>
                                            </div>

                                            <!-- <div class="form-group col-xs-12 col-sm-12">
                                                <label for="slogan_ro" class="col-sm-3 control-label no-padding-right"> Slogan Ro: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="slogan_ro" value="" required class="col-xs-12" placeholder="Slogan Ro" id="slogan_ro">
                                                </div>
                                            </div> -->

                                            <div class="col-xs-12" style="margin-top: 20px;border-top: 1px solid black">
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#input1" aria-expanded="true">
                                                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                                                Brand Design
                                                            </a>
                                                        </li>

                                                        <li class="">
                                                            <a data-toggle="tab" href="#input2" aria-expanded="false">
                                                                <i class="blue ace-icon fa fa-cogs bigger-110"></i>
                                                                Brand Behaviour
                                                            </a>
                                                        </li>

                                                        <li class="">
                                                            <a data-toggle="tab" href="#input3" aria-expanded="false">
                                                                <i class="ace-icon fa fa-rocket"></i>
                                                                Brand Information and Communication
                                                            </a>
                                                        </li>

                                                        <li class="">
                                                            <a data-toggle="tab" href="#input4" aria-expanded="false">
                                                                <i class="ace-icon fa-bullhorn red"></i>
                                                                Site Delivery/Exposure of brand features
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">

                                                        <div id="input1" class="tab-pane active">
                                                            <div class="col-xs-12 col-sm-6"
                                                                 style="border-right: 1px solid grey;">
                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND1_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Slogan: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND1_checkbox" title="Slogan"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND1"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND2_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Logo: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND2_checkbox" title="Logo"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND2"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND3_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Coat of Arms: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND3_checkbox"
                                                                               title="Coat of Arms"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND3"/>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND4_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Picure/ Image Gallery: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND4_checkbox"
                                                                               title="Picure/ Image Gallery"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND4"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND5_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Maps: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND5_checkbox" title="Maps"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND5"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CBRAND6_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right checkBoxSelector">
                                                                        Flag: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND6_checkbox" title="Flags"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CBRAND6"/>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div id="input2" class="tab-pane">
                                                            <div class="col-xs-12 col-sm-6"
                                                                 style="border-right: 1px solid grey;">
                                                                <div class="form-group col-xs-12">
                                                                    <label for="CMBRAND1_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        News (section)/ Public Notices: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CBRAND1_checkbox"
                                                                               title="News (section)/ Public Notices"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CMBRAND1"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CMBRAND2_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Events Calendar: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CMBRAND2_checkbox"
                                                                               title="Events Calendar"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CMBRAND2"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CMBRAND3_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Name of the authority under which the place
                                                                        brand exists: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CMBRAND3_checkbox"
                                                                               title="Name of the authority under which the place brand exists"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CMBRAND3"/>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group col-xs-12">
                                                                    <label for="CMBRAND4_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Board members of the authority: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CMBRAND4_checkbox"
                                                                               title="Board members of the authority"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CMBRAND4"/>
                                                                </div>

                                                                <div class="form-group col-xs-12">
                                                                    <label for="CMBRAND5_checkbox"
                                                                           class="col-sm-9 control-label no-padding-right align-right">
                                                                        Weather condition Plug-in: </label>

                                                                    <div class="col-sm-3">
                                                                        <input name="CMBRAND5_checkbox"
                                                                               title="Weather condition Plug-in"
                                                                               class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                               type="checkbox">
                                                                        <span class="lbl"></span>
                                                                    </div>
                                                                    <input type="hidden" name="CMBRAND5"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="input3" class="tab-pane">


                                                            <h4 class="widget-title">Information - Downward information
                                                                flows</h4>

                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">

                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFDESC1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            General information about the place (e.g.,
                                                                            history, main objectives,
                                                                            attraction): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFDESC_checkbox"
                                                                                   title="General information about the place (e.g., history, main objectives, attraction)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFDESC1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFDESC2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Customised information for target audinces
                                                                            (e.g., brochures): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFDESC2_checkbox"
                                                                                   title="Customised information for target audinces (e.g., brochures)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFDESC2"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFDESC3_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Public announcements(e.g., Press Releases,
                                                                            Newsletters): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFDESC3_checkbox"
                                                                                   title="Public announcements(e.g., Press Releases, Newsletters)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFDESC3"/>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFDESC4_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Frequently Asked Questions (F.A.Q. Travel
                                                                            Tips, Good to Know): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFDESC4_checkbox"
                                                                                   title="Frequently Asked Questions (F.A.Q. Travel Tips, Good to Know)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFDESC4"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFDESC5_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Contact information(address, phone number,
                                                                            e-mail address): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFDESC5_checkbox"
                                                                                   title="Contact information(address, phone number, e-mail address)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFDESC5"/>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <h4 class="widget-title">Information - Upward information
                                                                flows</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">
                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASC1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Offers (Rentals, shopping, reservations,
                                                                            orders): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASC1_checkbox"
                                                                                   title="Offers (Rentals, shopping, reservations, orders)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFASC1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASC2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Bookmark and Share: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASC2_checkbox"
                                                                                   title="Bookmark and Share"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFASC2"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASC3"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Cookies: </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="INFASC3" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="Cookies" id="INFASC3"
                                                                                   type="number">
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h4 class="widget-title">Information - Lateral/ horizontal
                                                                information flows</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">

                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFLAT1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Useful links: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFLAT1_checkbox"
                                                                                   title="Useful links"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFLAT1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFLAT2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Internal links: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFLAT2_checkbox"
                                                                                   title="Internal links"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INFLAT2"/>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <h4 class="widget-title">Interactivity - Interactive
                                                                information flows: asynchronous</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">

                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Option to download (e.g., pdf document,
                                                                            wallpaper, screen saver): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASINC1_checkbox"
                                                                                   title="Option to download (e.g., pdf document, wallpaper, screen saver)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC2_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Site search: </label>

                                                                        <div class="col-sm-3">
                                                                            <select class="form-control onchangeSelect">
                                                                                <option value="0">Lack of choice
                                                                                </option>
                                                                                <option value="1">Simple Search</option>
                                                                                <option value="2">Advanced Search
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC2" value="0"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC3_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Ways of contact: </label>

                                                                        <div class="col-sm-3">
                                                                            <select class="form-control onchangeSelect">
                                                                                <option value="0">Lack of choice
                                                                                </option>
                                                                                <option value="1">One e-mail address
                                                                                </option>
                                                                                <option value="2">Multiple e-mail
                                                                                    addresses
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC3" value="0"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC4_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Online form for feedback or
                                                                            contact: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASINC4_checkbox"
                                                                                   title="Online form for feedback or contact"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC4"/>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC5_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Subscribe to a service (e.g.,
                                                                            Newsletter): </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASINC5_checkbox"
                                                                                   title="Subscribe to a service (e.g., Newsletter)"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC5"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC6_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Sign up as a member: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASINC6_checkbox"
                                                                                   title="Sign up as a member"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC6"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INFASINC7_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Provide online poll feedback, other voting
                                                                            system: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INFASINC7_checkbox"
                                                                                   title="Provide online poll feedback, other voting system"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INASINC7"/>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <h4 class="widget-title">Interactivity - Interactive
                                                                information flows: synchronous</h4>
                                                            <div class="col-xs-12">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INSINC1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Live chat with staff members: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INSINC1_checkbox"
                                                                                   title="Live chat with staff members"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INSINC1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INSINC2_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Social media connection (e.g., Twitter,
                                                                            Facebook, Instagram, Google+,
                                                                            Pinteres): </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="INSINC2" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="Social media (e.g., Twitter, Facebook, Instagram, Google+, Pinteres)"
                                                                                   id="INSINC2_select" type="number">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="INSINC3_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Smart applications: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="INSINC3_checkbox"
                                                                                   title="Smart applications"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="INSINC3"/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div id="input4" class="tab-pane">
                                                            <h4 class="widget-title">Presentation and appearance</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">
                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="PREZ1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Intro component of site: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="PREZ1_checkbox"
                                                                                   title="Intro component of site"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="PREZ1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="PREZ2_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Flashiness (graphic emphasis) (e.g., .jpeg,
                                                                            .gif, .png) in Homepage: </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="PREZ2" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="Flashiness (graphic emphasis) (e.g., .jpeg, .gif, .png) in Homepage"
                                                                                   id="PREZ2_select" type="number">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="MULTIM1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Dynamism (multimedia properties) - presence
                                                                            of mobile icons, images or animated
                                                                            text: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="MULTIM1_checkbox"
                                                                                   title="Dynamism (multimedia properties) - presence of mobile icons, images or animated text"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="MULTIM1"/>
                                                                    </div>

                                                                </div>

                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="MULTIM2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Dynamism (multimedia properties) - presence
                                                                            of audio materials: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="MULTIM2_checkbox"
                                                                                   title="Dynamism (multimedia properties) - presence of audio materials"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="MULTIM2"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="MULTIM3_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Dynamism (multimedia properties) - presence
                                                                            of video materials: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="MULTIM3_checkbox"
                                                                                   title="Dynamism (multimedia properties) - presence of video materials"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="MULTIM3"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="MULTIM4_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Dynamism (multimedia properties) - Live
                                                                            transmission presence, Webcams: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="MULTIM4_checkbox"
                                                                                   title="Dynamism (multimedia properties) - Live transmission presence, Webcams"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="MULTIM4"/>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <h4 class="widget-title">Accesibility</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">
                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACCES1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Presence of the "text only" option for the
                                                                            entire site: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="ACCES1_checkbox"
                                                                                   title="Presence of the 'text only' option for the entire site"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="ACCES1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACCES2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Downloading or printing "text only" for
                                                                            documents: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="ACCES2_checkbox"
                                                                                   title="Downloading or printing 'text only' for documents"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="ACCES2"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACCES3_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Translations into other languages of the
                                                                            site: </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="ACCES3" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="Translations into other languages of the site"
                                                                                   id="ACCES3_select" type="number">
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACCES4_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Adapt site to recognized accesibility
                                                                            standards: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="ACCES4_checkbox"
                                                                                   title="Adapt site ro recognized accesibility standards"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="ACCES4"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACCES5_select"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            The size in Kb of the Homepage: </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="ACCES5" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="The size in Kb of the Homepage"
                                                                                   id="ACCES5_select" type="number">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h4 class="widget-title">Navigability</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">
                                                                <div class="col-xs-12 col-sm-6"
                                                                     style="border-right: 1px solid grey;">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="NAVIG1_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Tips to make navigation easier: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="NAVIG1_checkbox"
                                                                                   title="Tips to make navigation easier"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="NAVIG1"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="NAVIG2_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            The Homepage icon on each page: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="NAVIG2_checkbox"
                                                                                   title="The Homepage icon on each page"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="NAVIG2"/>
                                                                    </div>

                                                                </div>

                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="NAVIG3_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            The main menu bar on each page: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="NAVIG3_checkbox"
                                                                                   title="The main menu bar on each page"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="NAVIG3"/>
                                                                    </div>

                                                                    <div class="form-group col-xs-12">
                                                                        <label for="NAVIG4_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Map or site index: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="NAVIG4_checkbox"
                                                                                   title="Map or site index"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="NAVIG4"/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h4 class="widget-title">Freshness</h4>
                                                            <div class="col-xs-12"
                                                                 style="border-bottom: 1px solid grey">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="ACTUALIZ_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Last update of the site - presence of the
                                                                            feature in the Homepage: </label>

                                                                        <div class="col-sm-3">
                                                                            <input name="ACTUALIZ_checkbox"
                                                                                   title="Last update of the site - presence of the feature in the Homepage"
                                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                                   type="checkbox">
                                                                            <span class="lbl"></span>
                                                                        </div>
                                                                        <input type="hidden" name="ACTUALIZ"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <h4 class="widget-title">Visibility</h4>
                                                            <div class="col-xs-12">
                                                                <div class="col-xs-12 col-sm-6">
                                                                    <div class="form-group col-xs-12">
                                                                        <label for="VIZIBIL_checkbox"
                                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                                            Ease of localization on the internet -
                                                                            Number of links from other external
                                                                            sources: </label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text" name="VIZIBIL" value="0"
                                                                                   class="col-xs-12"
                                                                                   placeholder="Ease of localization on the internet - Number of linkgs form other externa sources"
                                                                                   id="VIZIBIL_select" type="number">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <button class="btn btn-sm btn-success" style="float:right;"
                                                        type="submit" id="addButton">
                                                    Add Site
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-header">User List:</div>
                            <div class="table-responive">
                                <table id="">
                                    <table id="categories-table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width:10%;text-align:center">#</th>
                                            <th>Name</th>
                                            <th>Url</th>
                                            <th>Country</th>
                                            <th>Continent</th>
                                            <th>Date</th>
                                            <th>Score Brand Design</th>
                                            <th>Score Brand Behaviour</th>
                                            <th>Score Brand Communication</th>
                                            <th>Score Site Delivery</th>
                                            <th>Total Score</th>
                                            <th>Active</th>
                                            <th style="width:100px;">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($sites as $key => $value): ?>
                                            <tr>
                                                <td style="text-align: center">
                                                    <?php echo $key + 1; ?>
                                                </td>

                                                <td>
                                                    <a href="view_site.php?id=<?php echo strip_tags($value->id); ?>">
                                                        <?php echo strip_tags($value->name_en); ?>
                                                    </a>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->url); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->country); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->continent); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->add_date); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_design); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_behaviour); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_communication); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->site_delivery); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->total_score); ?> pts
                                                </td>

                                                <td>
                                                    <?php

                                                    if ($value->active == 0) {
                                                        echo "Inactive";
                                                    } else {
                                                        echo "Active";
                                                    }

                                                    ?>
                                                </td>

                                                <td>
                                                    <div class="action-buttons">
                                                        <a href="view_site.php?id=<?php echo $value->id; ?>"
                                                           class="blue" style="float:left;">
                                                            <i class="ace-icon fa fa-search-plus bigger-130"></i>
                                                        </a>

                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="deleteSite" name="action"/>
                                                            <input type="hidden" value="<?php echo $value->id; ?>"
                                                                   name="id"/>
                                                            <input type="hidden" value="<?php echo $value->logo; ?>"
                                                                   name="logo"/>
                                                            <button type="submit">
                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<? include("./html_includes/footer.html"); ?>

<a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/custom.js"></script>
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/date-time/moment.min.js"></script>
<script src="dist/js/fullcalendar.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">;
    $(document).ready(function () {
        var oTable1 = $('#categories-table').dataTable({
            "aoColumns": [
                {"bSortable": false},
                null, null, null, null, null, null, null, null, null, null, null,
                {"bSortable": false}
            ]
        });

        $(document).ready(function () {
            $(document).on("click", ".checkBoxSelector", function () {
                var myVar = $(this).parent().siblings("input:first");
                if ($(this).is(":checked")) {
                    myVar.val("1");
                } else {
                    myVar.val("0")
                }

            });

            $(document).on("change", ".onchangeSelect", function () {
                var myVar = $(this).val();
                $(this).parent().siblings("input:first").val(myVar);
            });

        });

    });
</script>


</body>
</html>