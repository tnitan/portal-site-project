<?php

require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$user = getUser($_SESSION["user_id"]);
$sites = getUserSites($_SESSION["user_id"]);
// var_dump($sites);
// die();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>My Account - <?php echo $user->username ?> </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="active">
                <a href="my_account.php">
                    <i class="menu-icon fa 	fa-lemon-o  red"></i>
                    <span class="menu-text"> My Account </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="sites.php">
                    <i class="menu-icon fa 	fa-external-link  green"></i>
                    <span class="menu-text"> My Sites </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="active"><?php echo $user->first_name ?><?php echo $user->last_name ?></li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 3) {
                            echo "user";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    <?php echo strip_tags($user->username) ?>
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Manage user info
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="col-xs-12">
                        <div class="col-sm-7 col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">Edit <?php echo strip_tags($user->username) ?></h4>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main" style="overflow: hidden">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="./php_includes/post_switch.php">
                                            <input type="hidden" value="updateUser" name="action"/>
                                            <input type="hidden" value="<?php echo strip_tags($user->id) ?>" name="id"/>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="email" class="col-sm-3 control-label no-padding-right">
                                                    Email: </label>

                                                <div class="col-sm-9">
                                                    <input type="email" name="email" value="<?php echo $user->email ?>"
                                                           required class="col-xs-12" placeholder="Email" id="email">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="first_name" class="col-sm-3 control-label no-padding-right">
                                                    First Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="first_name"
                                                           value="<?php echo $user->first_name ?>" required
                                                           class="col-xs-12" placeholder="First Name" id="first_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="last_name" class="col-sm-3 control-label no-padding-right">
                                                    Last Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="last_name"
                                                           value="<?php echo $user->last_name ?>" required
                                                           class="col-xs-12" placeholder="Last Name" id="last_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="phone_number"
                                                       class="col-sm-3 control-label no-padding-right"> Phone
                                                    Number: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="phone_number"
                                                           value="<?php echo $user->phone_number ?>" class="col-xs-12"
                                                           placeholder="Phone Number" id="phone_number">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <button class="btn btn-sm btn-success" style="float:right;"
                                                        type="submit" id="addButton">
                                                    Update User
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-5 col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">Change My Password</h4>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main" style="overflow: hidden">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="./php_includes/post_switch.php">
                                            <input type="hidden" name="action" value="updatePassword">
                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="old_password"
                                                       class="col-sm-3 control-label no-padding-right"> Old
                                                    Password: </label>

                                                <div class="col-sm-9">
                                                    <input type="password" required name="old_password" type="password"
                                                           value="" class="col-xs-12" placeholder="Old password"
                                                           id="old_password">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="new_pass" class="col-sm-3 control-label no-padding-right">
                                                    New Password: </label>

                                                <div class="col-sm-9">
                                                    <input type="password" required name="new_pass" type="password"
                                                           value="" class="col-xs-12" placeholder="Old password"
                                                           id="new_pass">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="repeat_new_pass"
                                                       class="col-sm-3 control-label no-padding-right"> Repeat New
                                                    Password: </label>

                                                <div class="col-sm-9">
                                                    <input type="password" required name="repeat_new_pass"
                                                           type="password" value="" class="col-xs-12"
                                                           placeholder="Repeat new password password"
                                                           id="repeat_new_pass">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <button class="btn btn-sm btn-success" style="float:right;"
                                                        type="submit" id="updatePass">
                                                    Update Password
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    <? include("./html_includes/footer.html"); ?>

    <a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/custom.js"></script>
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/date-time/moment.min.js"></script>
<script src="dist/js/fullcalendar.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("blur", "#old_password", function () {
            var data = {
                action: "getOldPassword",
                old_password: $("#old_password").val()
            };

            $.post("./php_includes/ajax_functions.php",
                data,
                function(asd) {
                    if (asd === "false") {
                        $("#updatePass").attr("disabled","disabled");
                        $("#old_password").css("border-color","red");
                        alert("Old password invalid");
                    }
                    if (asd === "true") {
                        $("#updatePass").removeAttr("disabled");
                        $("#old_password").css("border-color","green");
                    }
                });

        });

        $(document).on("blur", "#new_pass", function () {
            if ($("#new_pass").val() !== $("#repeat_new_pass").val()) {
                $("#new_pass").css("border-color", "red");
                $("#repeat_new_pass").css("border-color", "red");

                $("#updatePass").attr("disabled", "disabled");

            } else {
                $("#new_pass").css("border-color", "green");
                $("#repeat_new_pass").css("border-color", "green");

                $("#updatePass").removeAttr("disabled");
            }
        });

        $(document).on("blur", "#repeat_new_pass", function () {
            if ($("#new_pass").val() !== $("#repeat_new_pass").val()) {
                $("#new_pass").css("border-color", "red");
                $("#repeat_new_pass").css("border-color", "red");

                $("#updatePass").attr("disabled", "disabled");

            } else {
                $("#new_pass").css("border-color", "green");
                $("#repeat_new_pass").css("border-color", "green");

                $("#updatePass").removeAttr("disabled");
            }
        });

    });
</script>


</body>
</html>