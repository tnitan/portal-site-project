<?php

require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$site = getSite($_GET["id"], $_GET["user_id"]);
$user = getUser($site->user_id);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Edit site : - <?php echo $site->name_en ?> </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="active">
                <a href="users.php">
                    <i class="menu-icon fa fa-users blue"></i>
                    <span class="menu-text"> Users </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="">
                        <i class="ace-icon fa fa-users"></i>
                        <a href="users.php">Users</a>
                    </li>
                    <li class="">
                        <i class="ace-icon fa fa-users"></i>
                        <a href="user.php?id=<?php echo $user->id ?>"><?php echo strip_tags($user->first_name) ?><?php echo strip_tags($user->last_name) ?></a>
                    </li>
                    <li class="active"><?php echo strip_tags($site->name_en) ?></li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 1) {
                            echo "admin";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    <?php echo $site->name_en ?>
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        View site details
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="col-xs-12">
                            <div class="col-sm-12 col-xs-12">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4 class="widget-title">View General Info for <?php echo strip_tags($site->name_en) ?></h4>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main" style="overflow: hidden">
                                            <table id="simple-table" class="table  table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th style="width:30%;">Parameter</th>
                                                    <th>Value</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td><?php echo strip_tags($site->name_en) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Date</td>
                                                    <td><?php echo strip_tags($site->add_date) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Url</td>
                                                    <td><?php echo strip_tags($site->url) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Country</td>
                                                    <td><?php echo strip_tags($site->country) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Continent</td>
                                                    <td><?php echo strip_tags($site->continent) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Slogan</td>
                                                    <td><?php echo strip_tags($site->slogan_en) ?></td>
                                                </tr>

                                                <tr>
                                                    <td>Status</td>
                                                    <td>
                                                        <strong>
                                                            <?php

                                                            if ($site->active == 0) {
                                                                echo "Inactive";
                                                            } else {
                                                                echo "Active";
                                                            }

                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <hr/>
                                <div class="widget-box">

                                    <div class="widget-header">
                                        <h4 class="widget-title">View/update analysis
                                            for <?php echo $site->name_en ?></h4>
                                    </div>

                                    <div class="tabbable tabs-left">
                                        <ul class="nav nav-tabs" id="myTab3">
                                            <li class="active">
                                                <a data-toggle="tab" href="#input1" aria-expanded="true">
                                                    <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                                    Brand Design
                                                </a>
                                            </li>

                                            <li class="">
                                                <a data-toggle="tab" href="#input2" aria-expanded="false">
                                                    <i class="blue ace-icon fa fa-cogs bigger-110"></i>
                                                    Brand Behaviour
                                                </a>
                                            </li>

                                            <li class="">
                                                <a data-toggle="tab" href="#input3" aria-expanded="false">
                                                    <i class="ace-icon fa fa-rocket"></i>
                                                    Brand Information and Comunication
                                                </a>
                                            </li>

                                            <li class="">
                                                <a data-toggle="tab" href="#input4" aria-expanded="false">
                                                    <i class="ace-icon fa-bullhorn red"></i>
                                                    Site Delivery/Exposure of brand features
                                                </a>
                                            </li>
                                        </ul>


                                        <div class="tab-content">

                                            <div id="input1" class="tab-pane active">
                                                <div class="col-xs-12 col-sm-6" style="border-right: 1px solid grey;">
                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND1_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Slogan: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND1_checkbox"
                                                                   title="Slogan"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND1 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND1"
                                                               value="<?php if ($site->CBRAND1 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND2_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Logo: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND2_checkbox"
                                                                   title="Logo"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND2 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND2"
                                                               value="<?php if ($site->CBRAND2 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND3_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Coat of Arms: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND3_checkbox"
                                                                   title="Coat of Arms"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND3 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND3"
                                                               value="<?php if ($site->CBRAND3 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND4_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Picure/ Image Gallery: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND4_checkbox"
                                                                   title="Picure/ Image Gallery"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND4 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND4"
                                                               value="<?php if ($site->CBRAND4 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND5_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Maps: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND5_checkbox"
                                                                   title="Maps"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND5 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND5"
                                                               value="<?php if ($site->CBRAND5 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CBRAND6_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right checkBoxSelector">
                                                            Flags: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND6_checkbox"
                                                                   title="Flags"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CBRAND6 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CBRAND6"
                                                               value="<?php if ($site->CBRAND6 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>
                                                </div>

                                            </div>

                                            <div id="input2" class="tab-pane">
                                                <div class="col-xs-12 col-sm-6" style="border-right: 1px solid grey;">
                                                    <div class="form-group col-xs-12">
                                                        <label for="CMBRAND1_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            News (section)/ Public Notices: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CBRAND1_checkbox"
                                                                   title="News (section)/ Public Notices"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CMBRAND1 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CMBRAND1"
                                                               value="<?php if ($site->CMBRAND1 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CMBRAND2_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Events Calendar: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CMBRAND2_checkbox"
                                                                   title="Events Calendar"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CMBRAND2 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CMBRAND2"
                                                               value="<?php if ($site->CMBRAND2 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CMBRAND3_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Name of the authority under which the place brand
                                                            exists: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CMBRAND3_checkbox"
                                                                   title="Name of the authority under which the place brand exists"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CMBRAND3 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CMBRAND3"
                                                               value="<?php if ($site->CMBRAND3 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group col-xs-12">
                                                        <label for="CMBRAND4_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Board members of the authority: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CMBRAND4_checkbox"
                                                                   title="Board members of the authority"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CMBRAND4 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CMBRAND4"
                                                               value="<?php if ($site->CMBRAND4 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>

                                                    <div class="form-group col-xs-12">
                                                        <label for="CMBRAND5_checkbox"
                                                               class="col-sm-9 control-label no-padding-right align-right">
                                                            Weather condition Plug-in: </label>

                                                        <div class="col-sm-3">
                                                            <input disabled="disabled" name="CMBRAND5_checkbox"
                                                                   title="Weather condition Plug-in"
                                                                   class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                   type="checkbox" <?php if ($site->CMBRAND5 == 1) {
                                                                echo 'checked="checked"';
                                                            }; ?> >
                                                            <span class="lbl"></span>
                                                        </div>
                                                        <input disabled="disabled" type="hidden" name="CMBRAND5"
                                                               value="<?php if ($site->CMBRAND5 == 1) {
                                                                   echo '1';
                                                               } else {
                                                                   echo '0';
                                                               } ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="input3" class="tab-pane">


                                                <h4 class="widget-title">Information - Downward information flows</h4>

                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">

                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFDESC1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                General information about the place (e.g., history, main
                                                                objectives, attraction): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFDESC_checkbox"
                                                                       title="General information about the place (e.g., history, main objectives, attraction)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFDESC1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFDESC1"
                                                                   value="<?php if ($site->INFDESC1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFDESC2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Customised information for target audinces (e.g.,
                                                                brochures): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFDESC2_checkbox"
                                                                       title="Customised information for target audinces (e.g., brochures)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFDESC2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFDESC2"
                                                                   value="<?php if ($site->INFDESC2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFDESC3_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Public announcements(e.g., Press Releases,
                                                                Newsletters): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFDESC3_checkbox"
                                                                       title="Public announcements(e.g., Press Releases, Newsletters)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFDESC3 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFDESC3"
                                                                   value="<?php if ($site->INFDESC3 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFDESC4_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Frequently Asked Questions (F.A.Q. Travel Tips, Good to
                                                                Know): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFDESC4_checkbox"
                                                                       title="Frequently Asked Questions (F.A.Q. Travel Tips, Good to Know)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFDESC4 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFDESC4"
                                                                   value="<?php if ($site->INFDESC4 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFDESC5_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Contact information(address, phone number, e-mail
                                                                address): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFDESC5_checkbox"
                                                                       title="Contact information(address, phone number, e-mail address)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFDESC5 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFDESC5"
                                                                   value="<?php if ($site->INFDESC5 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>

                                                </div>


                                                <h4 class="widget-title">Information - Upward information flows</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">
                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASC1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Offers (Rentals, shopping, reservations,
                                                                orders): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASC1_checkbox"
                                                                       title="Offers (Rentals, shopping, reservations, orders)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFASC1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFASC1"
                                                                   value="<?php if ($site->INFASC1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASC2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Bookmark and Share: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASC2_checkbox"
                                                                       title="Bookmark and Share"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFASC2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFASC2"
                                                                   value="<?php if ($site->INFASC2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASC3"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Cookies: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="INFASC3"
                                                                       value="<?php echo $site->INFASC3 ?>"
                                                                       class="col-xs-12" placeholder="Cookies"
                                                                       id="INFASC3" type="number">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="widget-title">Information - Lateral/ horizontal information
                                                    flows</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">

                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFLAT1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Useful links: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFLAT1_checkbox"
                                                                       title="Useful links"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFLAT1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFLAT1"
                                                                   value="<?php if ($site->INFLAT1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFLAT2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Internal links: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFLAT2_checkbox"
                                                                       title="Internal links"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INFLAT2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INFLAT2"
                                                                   value="<?php if ($site->INFLAT2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                    </div>

                                                </div>

                                                <h4 class="widget-title">Interactivity - Interactive information flows:
                                                    asynchronous</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">

                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Option to download (e.g., pdf document, wallpaper,
                                                                screen saver): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASINC1_checkbox"
                                                                       title="Option to download (e.g., pdf document, wallpaper, screen saver)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INASINC1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC1"
                                                                   value="<?php if ($site->INASINC1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC2_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Site search: </label>

                                                            <div class="col-sm-3">
                                                                <select class="form-control onchangeSelect">
                                                                    <option value="0" <?php if ($site->INASINC2 == 0) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >Lack of choice
                                                                    </option>
                                                                    <option value="1" <?php if ($site->INASINC2 == 1) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >Simple Search
                                                                    </option>
                                                                    <option value="2" <?php if ($site->INASINC2 == 2) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >Advanced Search
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC2"
                                                                   value="<?php echo $site->INASINC2; ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC3_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Ways of contact: </label>

                                                            <div class="col-sm-3">
                                                                <select class="form-control onchangeSelect">
                                                                    <option value="0" <?php if ($site->INASINC3 == 0) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >Lack of choice
                                                                    </option>
                                                                    <option value="1" <?php if ($site->INASINC3 == 1) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >One e-mail address
                                                                    </option>
                                                                    <option value="2" <?php if ($site->INASINC3 == 2) {
                                                                        echo 'selected="selected"';
                                                                    }; ?> >Multiple e-mail addresses
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC3"
                                                                   value="<?php echo $site->INASINC3; ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC4_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Online form for feedback or contact: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASINC4_checkbox"
                                                                       title="Online form for feedback or contact"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INASINC4 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC4"
                                                                   value="<?php if ($site->INASINC4 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC5_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Subscribe to a service (e.g., Newsletter): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASINC5_checkbox"
                                                                       title="Subscribe to a service (e.g., Newsletter)"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INASINC5 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC5"
                                                                   value="<?php if ($site->INASINC5 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC6_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Sign up as a member: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASINC6_checkbox"
                                                                       title="Sign up as a member"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INASINC6 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC6"
                                                                   value="<?php if ($site->INASINC6 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INFASINC7_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Provide online poll feedback, other voting
                                                                system: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INFASINC7_checkbox"
                                                                       title="Provide online poll feedback, other voting system"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INASINC7 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INASINC7"
                                                                   value="<?php if ($site->INASINC7 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                    </div>

                                                </div>

                                                <h4 class="widget-title">Interactivity - Interactive information flows:
                                                    synchronous</h4>
                                                <div class="col-xs-12">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="INSINC1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Live chat with staff members: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INSINC1_checkbox"
                                                                       title="Live chat with staff members"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INSINC1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INSINC1"
                                                                   value="<?php if ($site->INSINC1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INSINC2_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Social media (e.g., Twitter, Facebook, Instagram,
                                                                Google+, Pinteres): </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="INSINC2"
                                                                       value="<?php echo $site->INSINC2 ?>"
                                                                       class="col-xs-12"
                                                                       placeholder="Social media (e.g., Twitter, Facebook, Instagram, Google+, Pinteres)"
                                                                       id="INSINC2_select" type="number">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="INSINC3_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Smart applications: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="INSINC3_checkbox"
                                                                       title="Smart applications"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->INSINC3 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="INSINC3"
                                                                   value="<?php if ($site->INSINC3 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div id="input4" class="tab-pane">
                                                <h4 class="widget-title">Presentation and appearance</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">
                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="PREZ1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Intro component of site: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="PREZ1_checkbox"
                                                                       title="Intro component of site"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->PREZ1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="PREZ1"
                                                                   value="<?php if ($site->PREZ1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="PREZ2_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Flashiness (graphic emphasis) (e.g., .jpeg, .gif, .png)
                                                                in Homepage: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="PREZ2"
                                                                       value="<?php echo $site->PREZ2 ?>"
                                                                       class="col-xs-12"
                                                                       placeholder="Flashiness (graphic emphasis) (e.g., .jpeg, .gif, .png) in Homepage"
                                                                       id="PREZ2_select" type="number">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="MULTIM1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Dynamism (multimedia properties) - presence of mobile
                                                                icons, images or animated text: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="MULTIM1_checkbox"
                                                                       title="Dynamism (multimedia properties) - presence of mobile icons, images or animated text"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->MULTIM1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="MULTIM1"
                                                                   value="<?php if ($site->MULTIM1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="MULTIM2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Dynamism (multimedia properties) - presence of audio
                                                                materials: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="MULTIM2_checkbox"
                                                                       title="Dynamism (multimedia properties) - presence of audio materials"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->MULTIM2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="MULTIM2"
                                                                   value="<?php if ($site->MULTIM2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="MULTIM3_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Dynamism (multimedia properties) - presence of video
                                                                materials: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="MULTIM3_checkbox"
                                                                       title="Dynamism (multimedia properties) - presence of video materials"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->MULTIM3 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="MULTIM3"
                                                                   value="<?php if ($site->MULTIM3 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="MULTIM4_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Dynamism (multimedia properties) - Live transmission
                                                                presence, Webcams: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="MULTIM4_checkbox"
                                                                       title="Dynamism (multimedia properties) - Live transmission presence, Webcams"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->MULTIM2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="MULTIM4"
                                                                   value="<?php if ($site->MULTIM4 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                    </div>

                                                </div>

                                                <h4 class="widget-title">Accesibility</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">
                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="ACCES1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Presence of the "text only" option for the entire
                                                                site: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="ACCES1_checkbox"
                                                                       title="Presence of the 'text only' option for the entire site"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->ACCES1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="ACCES1"
                                                                   value="<?php if ($site->ACCES1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="ACCES2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Downloading or printing "text only" for
                                                                documents: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="ACCES2_checkbox"
                                                                       title="Downloading or printing 'text only' for documents"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->ACCES2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="ACCES2"
                                                                   value="<?php if ($site->ACCES2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="ACCES3_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Translations into other languages of the site: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="ACCES3"
                                                                       value="<?php echo $site->ACCES3 ?>"
                                                                       class="col-xs-12"
                                                                       placeholder="Translations into other languages of the site"
                                                                       id="ACCES3_select" type="number">
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="ACCES4_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Adapt site ro recognized accesibility
                                                                standards: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="ACCES4_checkbox"
                                                                       title="Adapt site ro recognized accesibility standards"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->ACCES4 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="ACCES4"
                                                                   value="<?php if ($site->ACCES4 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="ACCES5_select"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                The size in Kb of the Homepage: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="ACCES5"
                                                                       value="<?php echo $site->ACCES5 ?>"
                                                                       class="col-xs-12"
                                                                       placeholder="The size in Kb of the Homepage"
                                                                       id="ACCES5_select" type="number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="widget-title">Navigability</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">
                                                    <div class="col-xs-12 col-sm-6"
                                                         style="border-right: 1px solid grey;">
                                                        <div class="form-group col-xs-12">
                                                            <label for="NAVIG1_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Tips to make navigation easier: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="NAVIG1_checkbox"
                                                                       title="Tips to make navigation easier"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->NAVIG1 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="NAVIG1"
                                                                   value="<?php if ($site->NAVIG1 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="NAVIG2_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                The Homepage icon on each page: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="NAVIG2_checkbox"
                                                                       title="The Homepage icon on each page"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->NAVIG2 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="NAVIG2"
                                                                   value="<?php if ($site->NAVIG2 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                    </div>

                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="NAVIG3_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                The main menu bar on each page: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="NAVIG3_checkbox"
                                                                       title="The main menu bar on each page"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->NAVIG3 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="NAVIG3"
                                                                   value="<?php if ($site->NAVIG3 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>

                                                        <div class="form-group col-xs-12">
                                                            <label for="NAVIG4_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Map or site index: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="NAVIG4_checkbox"
                                                                       title="Map or site index"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->NAVIG4 == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="NAVIG4"
                                                                   value="<?php if ($site->NAVIG4 == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="widget-title">Freshness</h4>
                                                <div class="col-xs-12" style="border-bottom: 1px solid grey">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="ACTUALIZ_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Last update of the site - presence of the feature in the
                                                                Homepage: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" name="ACTUALIZ_checkbox"
                                                                       title="Last update of the site - presence of the feature in the Homepage"
                                                                       class="ace ace-switch ace-switch-2 checkBoxSelector"
                                                                       type="checkbox" <?php if ($site->ACTUALIZ == 1) {
                                                                    echo 'checked="checked"';
                                                                }; ?> >
                                                                <span class="lbl"></span>
                                                            </div>
                                                            <input disabled="disabled" type="hidden" name="ACTUALIZ"
                                                                   value="<?php if ($site->ACTUALIZ == 1) {
                                                                       echo '1';
                                                                   } else {
                                                                       echo '0';
                                                                   } ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="widget-title">Visibility</h4>
                                                <div class="col-xs-12">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group col-xs-12">
                                                            <label for="VIZIBIL_checkbox"
                                                                   class="col-sm-9 control-label no-padding-right align-right">
                                                                Ease of localization on the internet - Number of linkgs
                                                                form other externa sources: </label>

                                                            <div class="col-sm-3">
                                                                <input disabled="disabled" type="text" name="VIZIBIL"
                                                                       value="<?php echo strip_tags($site->VIZIBIL) ?>"
                                                                       class="col-xs-12"
                                                                       placeholder="Ease of localization on the internet - Number of linkgs form other externa sources"
                                                                       id="VIZIBIL_select" type="number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div>
            </div>
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include("./html_includes/footer.html"); ?>

<a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/custom.js"></script>
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/date-time/moment.min.js"></script>
<script src="dist/js/fullcalendar.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">;
    $(document).ready(function () {
        var oTable1 = $('#categories-table').dataTable({
            "aoColumns": [
                {"bSortable": false},
                null, null, null, null, null, null
                {"bSortable": false}
            ]
        });

    });
</script>


</body>
</html>