<?php
require_once("post_functions.php");

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'addUser':
            addUser();
            break;

        case 'deleteUser':
            deleteUser();
            break;

        case 'updateUserAdmin':
            updateUserAdmin();
            break;

        case 'disableUser':
            disableUser();
            break;

        case 'enableUser':
            enableUser();
            break;

        case 'enableSite':
            enableSite();
            break;

        case 'disableSite':
            disableSite();
            break;


        // default:
        // 	header("Location: ../index.php");
        // 	break;
    }
}
?>