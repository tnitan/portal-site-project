<?php
require_once("connection.php");
require_once("functions.php");
function generateUri($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function addUser()
{
    $conn = connectDB();

    $username = $_POST["username"];
    $password = md5($_POST["password"]);
    $type = $_POST["type"];
    $email = $_POST["email"];
    $phone_number = $_POST["phone_number"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];

    $query = "INSERT INTO users(username,password,type,email,phone_number,first_name,last_name) VALUES(:username,:password,:type,:email,:phone_number,:first_name,:last_name)";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":username", $username);
    $stmt->bindParam(":password", $password);
    $stmt->bindParam(":type", $type);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":phone_number", $phone_number);
    $stmt->bindParam(":first_name", $first_name);
    $stmt->bindParam(":last_name", $last_name);

    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function deleteUser()
{
    $conn = connectDB();

    $id = $_POST["id"];

    $query = "DELETE FROM users WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function updateUserAdmin()
{
    $conn = connectDB();

    $id = $_POST['id'];
    $username = $_POST["username"];
    if (strlen($_POST["password"]) > 0) {
        $password = md5($_POST["password"]);
        $flag = true;
    } else {
        $flag = false;
    }
    $type = $_POST["type"];
    $email = $_POST["email"];
    $phone_number = $_POST["phone_number"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];


    if ($flag) {
        $query = "UPDATE users SET username = :username, password = :password, type = :type, email = :email, phone_number = :phone_number, first_name = :first_name, last_name = :last_name WHERE id=:id";
    } else {
        $query = "UPDATE users SET username = :username, type = :type, email = :email, phone_number = :phone_number, first_name = :first_name, last_name = :last_name WHERE id=:id";
    }
    $stmt = $conn->prepare($query);

    $stmt->bindParam(":username", $username);
    if ($flag) {
        $stmt->bindParam(":password", $password);
    }
    $stmt->bindParam(":type", $type);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":phone_number", $phone_number);
    $stmt->bindParam(":first_name", $first_name);
    $stmt->bindParam(":last_name", $last_name);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../user.php?id=" . $id);
    die();
}

function disableUser()
{
    $conn = connectDB();

    $id = $_POST['id'];

    $query = "UPDATE users SET approved=0 WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function enableUser()
{
    $conn = connectDB();

    $id = $_POST['id'];

    $query = "UPDATE users SET approved=1 WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../users.php");
    die();
}

function disableSite()
{
    $conn = connectDB();

    $id = $_POST['id'];
    $user_id = $_POST['user_id'];

    $query = "UPDATE site SET active=0 WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../user.php?id=" . $user_id);
    die();
}

function enableSite()
{
    $conn = connectDB();

    $id = $_POST['id'];
    $user_id = $_POST['user_id'];

    $query = "UPDATE site SET active=1 WHERE id=:id";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(":id", $id);

    $stmt->execute();

    $conn = null;
    header("Location: ../user.php?id=" . $user_id);
    die();
}

?>