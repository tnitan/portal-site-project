<?php
session_start();

if (!isset($_SESSION['login_attempts'])) {
    $_SESSION['login_attempts'] = 0;
}

if (!isset($_SESSION['session_id'])) {
    $_SESSION['session_id'] = 0;
}

if (!isset($_SESSION['user_id'])) {
    $_SESSION['user_id'] = 0;
}

if (!isset($_SESSION['user_type'])) {
    $_SESSION['user_type'] = 0;
}
?>