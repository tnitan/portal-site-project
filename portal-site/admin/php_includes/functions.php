<?php
ini_set('display_errors', 1);
@session_start();

require_once("connection.php");

function checkConnectionAdmin()
{

    if (!isset($_SESSION['session_id']) || !isset($_SESSION['user_id']) || $_SESSION['session_id'] == "0" || $_SESSION['user_id'] == "0") {
        return 0;
    } else {
        $session_id = $_SESSION['session_id'];
        $user_id = $_SESSION['user_id'];

        $DB_session_id = getSessionIdDBAdmin($user_id);

        if ($DB_session_id == $session_id) {
            return 1;
        } else {
            return 0;
        }
    }
}

function getSessionIdDBAdmin($id)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "SELECT session_id FROM admin WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $session_id = $stmt->fetch(PDO::FETCH_ASSOC);
    $conn = null;

    return $session_id['session_id'];

}

function loginForm($username, $password)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "SELECT id,username,session_id,user_type FROM `admin` WHERE username=:username AND password=:password";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":username", $username);
    $stmt->bindParam(":password", $password);
    $stmt->execute();
    if ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $new_session_id = generateNewSessionId();

        updateSessionId($new_session_id, $result['id']);
        $infoArray = $result;
        $infoArray['session_id'] = $new_session_id;

        return $infoArray;
    } else {
        return 1;
    }
}

function generateNewSessionId()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < 24; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function gen_slug($str)
{
    # special accents
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
    return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
}

function updateSessionId($session_id, $id)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "UPDATE admin SET session_id=:session_id WHERE id=:id";

    $stmt = $conn->prepare($query);
    $stmt->bindParam(":session_id", $session_id);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $conn = null;
}

function getUsers()
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "SELECT * FROM users";
    $stmt = $conn->prepare($query);

    $stmt->execute();

    $results = array();

    while ($result = $stmt->fetch(PDO::FETCH_OBJ)) {
        $results[] = $result;
    }

    $conn = null;

    return $results;
    exit();
}

function getUser($id)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }


    $query = "SELECT * FROM users WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    if ($user = $stmt->fetch(PDO::FETCH_OBJ)) {
        $result = $user;
        $conn = null;

        return $result;
        die();
    } else {
        $conn = null;

        header("Location: users.php");
        die();
    }
}

function getAllSites()
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "SELECT * FROM site";
    $stmt = $conn->prepare($query);

    $stmt->execute();

    $results = array();

    while ($result = $stmt->fetch(PDO::FETCH_OBJ)) {
        $results[] = $result;
    }

    $conn = null;

    return $results;
    exit();
}

function getUserSites($user_id)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }

    $query = "SELECT * FROM site WHERE user_id=:user_id";
    $stmt = $conn->prepare($query);

    $stmt->bindParam(":user_id", $user_id);

    $stmt->execute();

    $results = array();

    while ($result = $stmt->fetch(PDO::FETCH_OBJ)) {
        $results[] = $result;
    }

    $conn = null;

    return $results;
    exit();
}

function getSite($id, $user_id)
{
    $conn = connectDB();

    if (is_null($conn)) {
        return null;
    }


    $query = "SELECT * FROM site WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    if ($site = $stmt->fetch(PDO::FETCH_OBJ)) {
        $result = $site;
        $conn = null;

        return $result;
        die();
    } else {
        $conn = null;

        header("Location: user.php?id=" + $user_id);
        die();
    }
}

require_once("post_functions.php");
require_once("post_switch.php");
?>
