<?php
require_once('session.php');
require_once('connection.php');
require_once('functions.php');


if ($_SESSION['login_attempts'] < 3) {
    if (isset($_POST['username']) || isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = md5($_POST['password']);

        $status = loginForm($username, $password);

        if (is_array($status)) {
            $_SESSION['user_id'] = $status['id'];
            $_SESSION['session_id'] = $status['session_id'];
            $_SESSION['username'] = $status['username'];
            $_SESSION['user_type'] = $status['user_type'];
            header('Location: ../index.php');
            exit();
        }

        $_SESSION['login_attempts']++;
        header("Location: ../login.php?errorStatus=" . $status);
        exit();
    }
} else {
    echo 'Brute force!';
}


?>