// NAV ANIMATION
$(document).ready(function(){
	showGreen=function(){
		$("#greenList").show();
		$("#redList").hide();
		$("#orangeList").hide();
		$("#blueList").hide();

		// console.log("green");
	}

	showRed=function(){
		$("#redList").show();
		$("#greenList").hide();
		$("#orangeList").hide();
		$("#blueList").hide();

		// console.log("red");
	}

	showOrange=function(){
		$("#orangeList").show();
		$("#greenList").hide();
		$("#redList").hide();
		$("#blueList").hide();

		// console.log("orange");
	}

	showBlue=function(){
		$("#blueList").show();
		$("#greenList").hide();
		$("#redList").hide();
		$("#orangeList").hide();

		// console.log("blue");
	}

})
// END NAV ANIMATION!


$(document).on("change","#duration_",function(){
	var value=$(this).val();

	$(".div_option").hide();

	$("#"+value).show();

	if(value == "clicks_option"){
		$("#duration_type").val("1");

		$("#ends_after_").attr("required","required");

		$("#datepicker1").removeAttr("required");
		$("#timepicker1").removeAttr("required");

	}

	if(value == "days_option"){
		$("#duration_type").val("2");
		
		$("#datepicker1").attr("required","required");
		$("#timepicker1").attr("required","required");

		$("#ends_after_").removeAttr("required");
	}

	if(value == "infinte_option"){
		$("#duration_type").val("3");

		$("#datepicker1").removeAttr("required");
		$("#timepicker1").removeAttr("required");
		$("#ends_after_").removeAttr("required");
	}
});

$(document).on("change","#timer_",function(){
	var value=$(this).val();

	$("#timer_id").val(value);
})

$(document).on("change","#addTypeSelect",function(){
	var value=$(this).val();

	$("#addTypeInput").val(value);
})

$(document).on("change",".editSelectType",function(){
	var value=$(this).val();
	$(this).siblings("input").val(value);
})

$(document).on("click",".cheat_link_checkbox",function(){
	var value=$(this).val();
	$("#activeInput").val(value);
})

$(document).on("change",".statusAd_",function(){
	var value=$(this).val();
	$("#statusAd").val(value);
})

$(document).on("click","#is_cheat_link_",function(){
	if($(this).is(":checked")){
		$("#is_cheat_link").val("1");
	}else{
		$("#is_cheat_link").val("0");
	}
})


