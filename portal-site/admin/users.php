<?php

require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$users = getUsers();
// var_dump($users);
// die();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Users - Portal </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="active">
                <a href="users.php">
                    <i class="menu-icon fa fa-users blue"></i>
                    <span class="menu-text"> Users </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="active">Users</li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 1) {
                            echo "admin";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    Users
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Manage database users
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="col-xs-12">
                        <div class="col-sm-12 col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">Add New Users</h4>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main" style="overflow: hidden">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="./php_includes/post_switch.php">
                                            <input type="hidden" value="addUser" name="action"/>
                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="username" class="col-sm-3 control-label no-padding-right">
                                                    Username: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="username" value="" required
                                                           class="col-xs-12" placeholder="Username" id="username">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="password" class="col-sm-3 control-label no-padding-right">
                                                    Password: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="password" value="" required
                                                           class="col-xs-12" placeholder="Password" id="password">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="type" class="col-sm-3 control-label no-padding-right">
                                                    Type: </label>

                                                <div class="col-sm-4 col-md-3 col-lg-3">
                                                    <input type="number" min="2" max="3" required name="type" value=""
                                                           class="col-xs-12" placeholder="2-Moderator || 3-User"
                                                           id="type">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="email" class="col-sm-3 control-label no-padding-right">
                                                    Email: </label>

                                                <div class="col-sm-9">
                                                    <input type="email" name="email" value="" required class="col-xs-12"
                                                           placeholder="Email" id="email">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="first_name" class="col-sm-3 control-label no-padding-right">
                                                    First Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="first_name" value="" required
                                                           class="col-xs-12" placeholder="First Name" id="first_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="last_name" class="col-sm-3 control-label no-padding-right">
                                                    Last Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="last_name" value="" required
                                                           class="col-xs-12" placeholder="Last Name" id="last_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="phone_number"
                                                       class="col-sm-3 control-label no-padding-right"> Phone
                                                    Number: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="phone_number" value="" class="col-xs-12"
                                                           placeholder="Phone Number" id="phone_number">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <button class="btn btn-sm btn-success" style="float:right;"
                                                        type="submit" id="addButton">
                                                    Add User
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-header">User List:</div>
                            <div class="table-responive">
                                <table id="">
                                    <table id="categories-table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width:10%;text-align:center">#</th>
                                            <th>Username</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Type</th>
                                            <th>Approved</th>
                                            <th style="width:100px;">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($users as $key => $value): ?>
                                            <tr>
                                                <td style="text-align: center">
                                                    <?php echo $key + 1; ?>
                                                </td>

                                                <td>
                                                    <a href="user.php?id=<?php echo strip_tags($value->id); ?>">
                                                        <?php echo strip_tags($value->username); ?>
                                                    </a>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->first_name); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->last_name); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->email); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->phone_number); ?>
                                                </td>

                                                <td>
                                                    <?php

                                                    if ($value->type == 2) {
                                                        echo "<strong>Moderator</strong>";
                                                    } else {
                                                        echo "<strong>User</strong>";
                                                    }

                                                    ?>
                                                </td>

                                                <td>
                                                    <?php

                                                    if ($value->approved == 0) {
                                                        echo "<button class='btn btn-warning'>Rejected</button>";
                                                    } else {
                                                        echo "<button class='btn btn-success'>Approved</button>";
                                                    }

                                                    ?>
                                                    <?php if ($value->approved == 0): ?>
                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="enableUser" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <button class="btn btn-success">Enable User</button>
                                                        </form>
                                                    <?php endif; ?>

                                                    <?php if ($value->approved != 0): ?>
                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="disableUser" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <button class="btn btn-warning">Disable User</button>
                                                        </form>
                                                    <?php endif; ?>
                                                </td>

                                                <td>
                                                    <div class="action-buttons">
                                                        <a href="user.php?id=<?php echo $value->id; ?>" class="blue"
                                                           style="float:left;">
                                                            <i class="ace-icon fa fa-search-plus bigger-130"></i>
                                                        </a>

                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="deleteUser" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <button type="submit">
                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include("./html_includes/footer.html"); ?>

<a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/custom.js"></script>
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/date-time/moment.min.js"></script>
<script src="dist/js/fullcalendar.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">;
    $(document).ready(function () {
        var oTable1 = $('#categories-table').dataTable({
            "aoColumns": [
                {"bSortable": false},
                null, null, null, null, null, null, null,
                {"bSortable": false}
            ]
        });

    });
</script>


</body>
</html>