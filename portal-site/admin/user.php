<?php

require_once("./global_includes/header.php");

$connected = checkConnectionAdmin();
if ($connected == 0) {
    header('Location: ./login.php');
    exit();
}

$user = getUser($_GET["id"]);
$sites = getUserSites($_GET["id"]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Users - <?php echo $user->username ?> </title>

    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="dist/css/fullcalendar.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="dist/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="dist/js/html5shiv.min.js"></script>
    <script src="dist/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default">
    <?php include("./html_includes/header.html"); ?>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive">
        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="nav nav-list">
            <li class="">
                <a href="index.php">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="active">
                <a href="users.php">
                    <i class="menu-icon fa fa-users blue"></i>
                    <span class="menu-text"> Users </span>
                </a>

                <b class="arrow"></b>
            </li>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
               data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <script type="text/javascript">
            try {
                ace.settings.check('sidebar', 'collapsed')
            } catch (e) {
            }
        </script>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="">
                        <i class="ace-icon fa fa-users"></i>
                        <a href="users.php">Users</a>
                    </li>
                    <li class="active"><?php echo $user->first_name ?><?php echo strip_tags($user->last_name) ?></li>
                </ul><!-- /.breadcrumb -->
                <div class="nav-search" id="nav-search">
                    ...you are <strong>

                        <?php
                        if ($_SESSION["user_type"] == 1) {
                            echo "admin";
                        }
                        ?>

                    </strong>
                </div>
            </div>

            <div class="page-header">
                <h1>
                    <?php echo $user->username ?>
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Manage user info
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="col-xs-12">
                        <div class="col-sm-12 col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">Edit <?php echo strip_tags($user->username) ?></h4>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main" style="overflow: hidden">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="./php_includes/post_switch.php">
                                            <input type="hidden" value="updateUserAdmin" name="action"/>
                                            <input type="hidden" value="<?php echo $user->id ?>" name="id"/>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="username" class="col-sm-3 control-label no-padding-right">
                                                    Username: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="username"
                                                           value="<?php echo $user->username ?>" required
                                                           class="col-xs-12" placeholder="Username" id="username">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="password" class="col-sm-3 control-label no-padding-right">
                                                    Password: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="password" value="" class="col-xs-12"
                                                           placeholder="Password" id="password">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="type" class="col-sm-3 control-label no-padding-right">
                                                    Type: </label>

                                                <div class="col-sm-4 col-md-3 col-lg-3">
                                                    <input type="number" min="2" max="3" required name="type"
                                                           value="<?php echo $user->type ?>" class="col-xs-12"
                                                           placeholder="2-Moderator || 3-User" id="type">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="email" class="col-sm-3 control-label no-padding-right">
                                                    Email: </label>

                                                <div class="col-sm-9">
                                                    <input type="email" name="email" value="<?php echo $user->email ?>"
                                                           required class="col-xs-12" placeholder="Email" id="email">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="first_name" class="col-sm-3 control-label no-padding-right">
                                                    First Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="first_name"
                                                           value="<?php echo $user->first_name ?>" required
                                                           class="col-xs-12" placeholder="First Name" id="first_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="last_name" class="col-sm-3 control-label no-padding-right">
                                                    Last Name: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="last_name"
                                                           value="<?php echo $user->last_name ?>" required
                                                           class="col-xs-12" placeholder="Last Name" id="last_name">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <label for="phone_number"
                                                       class="col-sm-3 control-label no-padding-right"> Phone
                                                    Number: </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="phone_number"
                                                           value="<?php echo $user->phone_number ?>" class="col-xs-12"
                                                           placeholder="Phone Number" id="phone_number">
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12">
                                                <button class="btn btn-sm btn-success" style="float:right;"
                                                        type="submit" id="addButton">
                                                    Update User
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-header">User Sites List:</div>
                            <div class="table-responive">
                                <table id="">
                                    <table id="categories-table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width:10%;text-align:center">#</th>
                                            <th>Name</th>
                                            <th>Url</th>
                                            <th>Country</th>
                                            <th>Continent</th>
                                            <th>Date</th>
                                            <th>Score Brand Design</th>
                                            <th>Score Brand Behaviour</th>
                                            <th>Score Brand Communication</th>
                                            <th>Score Site Delivery</th>
                                            <th>Total Score</th>
                                            <th>Active</th>
                                            <th style="width:100px;">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($sites as $key => $value): ?>
                                            <tr>
                                                <td style="text-align: center">
                                                    <?php echo $key + 1; ?>
                                                </td>

                                                <td>
                                                    <a href="view_site.php?id=<?php echo $value->id; ?>&user_id=<?php echo $_GET["id"] ?>">
                                                        <?php echo strip_tags($value->name_en); ?>
                                                    </a>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->url); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->country); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->continent); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->add_date); ?>
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_design); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_behaviour); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->brand_communication); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->site_delivery); ?> pts
                                                </td>

                                                <td>
                                                    <?php echo strip_tags($value->total_score); ?> pts
                                                </td>

                                                <td>
                                                    <?php

                                                    if ($value->active == 0) {
                                                        echo "<button class='btn btn-warning'>Rejected</button>";
                                                    } else {
                                                        echo "<button class='btn btn-success'>Approved</button>";
                                                    }

                                                    ?>
                                                    <?php if ($value->active == 0): ?>
                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="enableSite" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->user_id); ?>"
                                                                   name="user_id"/>
                                                            <button class="btn btn-success">Enable Site</button>
                                                        </form>
                                                    <?php endif; ?>

                                                    <?php if ($value->active != 0): ?>
                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="disableSite" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->user_id); ?>"
                                                                   name="user_id"/>
                                                            <button class="btn btn-warning">Disable Site</button>
                                                        </form>
                                                    <?php endif; ?>


                                                </td>

                                                <td>
                                                    <div class="action-buttons">
                                                        <a href="view_site.php?id=<?php echo $value->id; ?>&user_id=<?php echo $_GET["id"] ?>"
                                                           class="blue" style="float:left;">
                                                            <i class="ace-icon fa fa-search-plus bigger-130"></i>
                                                        </a>

                                                        <form method="POST" action="./php_includes/post_switch.php"
                                                              style="float:left">
                                                            <input type="hidden" value="deleteSite" name="action"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->id); ?>"
                                                                   name="id"/>
                                                            <input type="hidden" value="<?php echo strip_tags($value->logo); ?>"
                                                                   name="logo"/>
                                                            <button type="submit">
                                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include("./html_includes/footer.html"); ?>

<a href="blank.html#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='dist/js/jquery1x.min.js'>" + "<" + "/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='dist/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="dist/js/custom.js"></script>
<script src="dist/js/jquery-ui.custom.min.js"></script>
<script src="dist/js/jquery.ui.touch-punch.min.js"></script>
<script src="dist/js/date-time/moment.min.js"></script>
<script src="dist/js/fullcalendar.min.js"></script>
<script src="dist/js/bootbox.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables/jquery.dataTables.bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="dist/js/ace-elements.min.js"></script>
<script src="dist/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">;
    $(document).ready(function () {
        var oTable1 = $('#categories-table').dataTable({
            "aoColumns": [
                {"bSortable": false},
                null, null, null, null, null, null, null, null, null, null, null,
                {"bSortable": false}
            ]
        });

    });
</script>


</body>
</html>