-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17 Noi 2018 la 16:48
-- Versiune server: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portal_site`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `session_id` varchar(24) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `session_id`, `user_type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'mTAC4yC5qdEKHKxa7zaMcpdu', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_date` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `name_ro` text NOT NULL,
  `name_en` text NOT NULL,
  `country` text NOT NULL,
  `continent` text NOT NULL,
  `slogan_ro` text NOT NULL,
  `slogan_en` text NOT NULL,
  `logo` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `CBRAND1` int(12) NOT NULL DEFAULT '0',
  `CBRAND2` int(12) NOT NULL DEFAULT '0',
  `CBRAND3` int(12) NOT NULL DEFAULT '0',
  `CBRAND4` int(12) NOT NULL DEFAULT '0',
  `CBRAND5` int(12) NOT NULL DEFAULT '0',
  `CBRAND6` int(12) NOT NULL DEFAULT '0',
  `CMBRAND1` int(12) NOT NULL DEFAULT '0',
  `CMBRAND2` int(12) NOT NULL DEFAULT '0',
  `CMBRAND3` int(12) NOT NULL DEFAULT '0',
  `CMBRAND4` int(12) NOT NULL DEFAULT '0',
  `CMBRAND5` int(12) NOT NULL DEFAULT '0',
  `INFDESC1` int(12) NOT NULL DEFAULT '0',
  `INFDESC2` int(12) NOT NULL DEFAULT '0',
  `INFDESC3` int(12) NOT NULL DEFAULT '0',
  `INFDESC4` int(12) NOT NULL DEFAULT '0',
  `INFDESC5` int(12) NOT NULL DEFAULT '0',
  `INFASC1` int(12) NOT NULL DEFAULT '0',
  `INFASC2` int(12) NOT NULL DEFAULT '0',
  `INFASC3` int(12) NOT NULL DEFAULT '0',
  `INFLAT1` int(12) NOT NULL DEFAULT '0',
  `INFLAT2` int(12) NOT NULL DEFAULT '0',
  `INASINC1` int(12) NOT NULL DEFAULT '0',
  `INASINC2` int(12) NOT NULL DEFAULT '0',
  `INASINC3` int(12) NOT NULL DEFAULT '0',
  `INASINC4` int(12) NOT NULL DEFAULT '0',
  `INASINC5` int(12) NOT NULL DEFAULT '0',
  `INASINC6` int(12) NOT NULL DEFAULT '0',
  `INASINC7` int(12) NOT NULL DEFAULT '0',
  `INSINC1` int(12) NOT NULL DEFAULT '0',
  `INSINC2` int(12) NOT NULL DEFAULT '0',
  `INSINC3` int(12) NOT NULL DEFAULT '0',
  `PREZ1` int(12) NOT NULL DEFAULT '0',
  `PREZ2` int(12) NOT NULL DEFAULT '0',
  `MULTIM1` int(12) NOT NULL DEFAULT '0',
  `MULTIM2` int(12) NOT NULL DEFAULT '0',
  `MULTIM3` int(12) NOT NULL DEFAULT '0',
  `MULTIM4` int(12) NOT NULL DEFAULT '0',
  `ACCES1` int(12) NOT NULL DEFAULT '0',
  `ACCES2` int(12) NOT NULL DEFAULT '0',
  `ACCES3` int(12) NOT NULL DEFAULT '0',
  `ACCES4` int(12) NOT NULL DEFAULT '0',
  `ACCES5` int(12) NOT NULL DEFAULT '0',
  `NAVIG1` int(12) NOT NULL DEFAULT '0',
  `NAVIG2` int(12) NOT NULL DEFAULT '0',
  `NAVIG3` int(12) NOT NULL DEFAULT '0',
  `NAVIG4` int(12) NOT NULL DEFAULT '0',
  `ACTUALIZ` int(12) NOT NULL DEFAULT '0',
  `VIZIBIL` int(12) NOT NULL DEFAULT '0',
  `brand_design` int(12) NOT NULL DEFAULT '0',
  `brand_behaviour` int(12) NOT NULL DEFAULT '0',
  `brand_communication` int(12) NOT NULL DEFAULT '0',
  `site_delivery` int(12) NOT NULL DEFAULT '0',
  `total_score` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `session_id` varchar(24) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `email` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone_number` text NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restrictii pentru tabele sterse
--

--
-- Restrictii pentru tabele `site`
--
ALTER TABLE `site`
  ADD CONSTRAINT `site_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
