* PHP 5.6

* Add to ```hosts``` file in ```C:\Windows\System32\drivers\etc```:
```
127.0.0.1 portal-site.user
127.0.0.1 portal-site.admin
```

XAMPP with PHP 5.6 ```https://www.apachefriends.org/xampp-files/5.6.39/xampp-win32-5.6.39-0-VC11-installer.exe```
* Conf files described below can also be found in project root - folder ```xampp```

* Uncomment the following lines in ```httpd.conf``` if not uncommented already:
```
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule ssl_module modules/mod_ssl.so

Include conf/extra/httpd-vhosts.conf
```

* Rewrite ```httpd-ssl.conf``` contents with:
```
Listen 443
SSLCipherSuite HIGH:MEDIUM:!MD5:!RC4
SSLProxyCipherSuite HIGH:MEDIUM:!MD5:!RC4
SSLHonorCipherOrder on
SSLProtocol all -SSLv3
SSLProxyProtocol all -SSLv3
SSLPassPhraseDialog  builtin
SSLSessionCache "shmcb:C:/xampp/apache/logs/ssl_scache(512000)"
SSLSessionCacheTimeout  300
```

* Add to ```httpd-vhosts.conf``` file in ```/xampp/apache/conf/extra```
```
<VirtualHost *:80>
    DocumentRoot "/portal-site/user/"
    ServerName portal-site.user
    ErrorLog "logs/portal-site-user-error.log"
    CustomLog "logs/portal-site-user-access.log" common
    <Directory "/portal-site/user">
        SSLRequireSSL
        Allow from all
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot "/portal-site/user/"
    ServerName portal-site.user
    ErrorLog "logs/portal-site-user-error.log"
    CustomLog "logs/portal-site-user-access.log" common
    SSLEngine on
    SSLCertificateFile "/portal-site/user.crt"
    SSLCertificateKeyFile "/portal-site/user.key"
    <Directory "/portal-site/user">
        Allow from all
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/portal-site/admin/"
    ServerName portal-site.admin
    ErrorLog "logs/portal-site-admin-error.log"
    CustomLog "logs/portal-site-admin-access.log" common
    <Directory "/portal-site/admin">
        SSLRequireSSL
        Allow from all
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot "/portal-site/admin/"
    ServerName portal-site.admin
    ErrorLog "logs/portal-site-admin-error.log"
    CustomLog "logs/portal-site-admin-access.log" common
    SSLEngine on
    SSLCertificateFile "/portal-site/admin.crt"
    SSLCertificateKeyFile "/portal-site/admin.key"
    <Directory "/portal-site/admin">
    	Allow from all
    	Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```

* Set db info in ```/portal-site/user/php_includes/connection.php``` and ```/portal-site/admin/php_includes/connection.php```
* Import ```portal_site.sql```

* Admin zone accesible at ```https://portal-site.admin/``` with credentials: ```admin/admin```
* User zone at ```https://portal-site.user/```, but users must be created first